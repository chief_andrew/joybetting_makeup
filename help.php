<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('main_sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<h1>Помощь</h1>
				<form action="" method="post" class="form-help">
					<div class="container-fluid">
						<h2>Напишите нам</h2>
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label for="user_name">Ваше имя</label>
									<input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter yor name">
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label for="user_email">Ваш электронный адрес</label>
									<input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email">
								</div>							
							</div>
							<div class="col-xs-12">
								<div class="form-group">
									<label for="massage_text">Ваш сообщение</label>
									<textarea name="massage_text" class="form-control" id="massage_text" rows="5"></textarea>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<button class="btn btn-primary btn-lg btn-block" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Отправить</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="faq">
					<h2>Часто задаваемые вопросы</h2>
					<div class="response-block">
						<h3>Обязательно ли регистрироваться на сайте?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Да. При регистрации вам будет доступен личный кабинет, куда и будут приходить прогнозы.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Забыл пароль от учетной записи. Что делать?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Вы можете восстановить пароль к своей учетной записи, нажав в правом верхнем углу на надпись “Забыли пароль?” Дальше нужно лишь следовать дальнейшим инструкциям.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Какая проходимость и средний коэффициент по вашим прогнозам?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Проходимость прогнозов около 70%, проходимость всего блока - 99,8%. Такого вы не найдете нигде, ну а средний коэффициент может варьироваться в диапазоне 1,5 - 2,5.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Как давно вы работаете?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Наша команда экспертов работает уже без малого 5 лет, за это время мы перепробовали множество вариантов сотрудничества с любителями ставок, стремящимися зарабатывать на жизнь ставками на спорт, пока не остановились на Пакетной системе, доказавшей свою наибольшую эффективность.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Что такое Пакет?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Под Пакетом здесь понимается совокупность прогнозов, после которых игрок гарантированно оказывается в плюсе на сумму, равной стоимости самого Пакета, но без учета платы за него.</p>
							<p>Например, наш Пакет стоит 1000 рублей, значит инвестировав эту сумму, наш Клиент будет получать прогнозы до тех пор, пока не окажется в плюсе на 2000 рублей, из которых 1000 - это возврат потраченных на Пакет денег, остальная 1000 - чистый плюс игрока.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Как я узнаю, что эти прогнозы качественные, и вообще, зачем мне вам доверять?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Вы можете посмотреть на раздел “Статистика” на нашем сайте и увидеть, что недовольных клиентов у нас еще не было. Ну а чтобы убедиться в этом самостоятельно, наш пакет ставок стоит всего 1000 рублей - небольшая цена за ресурс, с помощью которого, вы сможете зарабатывать еще много лет.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Неужели это тот же самый супер-, пупер-, мегажелезобетон, с проходимостью от 90%, которым забиты похожие сайты-конкуренты, но почему-то таковым не являющийся?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Многие наши конкуренты лишь косят под профессионалов, являясь банальными мошенниками. Мегажелезобетона попросту нету в природе (кроме ставок игроков-спортсменов против себя), хорошей считается проходимость около 70%.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Так почему же у вас статистика вся светится зеленым цветом? Что, совсем нет проигрышей? Это же противоречит предыдущему утверждению, что нету в природе 100% проходимости ставок.<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Статистика показывает проходимость Пакетов, а не отдельных ставок. Дело в том, что когда мы берем у игрока деньги, мы сразу же берем на себя обязательство сделать так,
чтобы у игрока была прибыль не меньше той, которую получили мы в качестве оплаты за Пакет. Т.е., в случае проигрыша ставки, мы бесплатно выдаем ему прогнозы до тех пор, пока наш клиент не получит эквивалентную прибыль по итогам всех ставок.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Неужели вам выгодно бесплатно раздавать еще кучу прогнозов, чем просто продавать один прогноз по фиксированной цене, как делают на других сайтах. Ведь если он качественный, клиент все равно будет доволен.<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Какими бы качественными прогнозы не были, увы бывает так, что клиент заплатил деньги, а результата не получил. И тут же начинает распространять негатив в соцсетях. Игрокам нужен результат. Здесь и сейчас. Получив его, такой игрок быстро становится нашим постоянным клиентом. А это куда выгоднее, чем тратиться на рекламу в поисках новых.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Если все так хорошо, как вы говорите и проходимость Пакетов зашкаливает, зачем вам деньги других игроков. Вы ведь сами можете зарабатывать много, ни с кем при этом не делясь.<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Увы. Букмекерский бизнес построен таким образом, что зарабатывать много и постоянно нам не дадут, административно снижая при этом максимумы на ставках, а то и вовсе блокируя счета. Одно дело, когда играет и выигрывает один игрок с малым оборотом, другое дело когда целая компания начнет вымывать сотни тысяч из контор на постоянной основе. Это мало кому может понравиться, поэтому мы и создаем дополнительный оборот за счет других игроков, делясь естественно при этом своей прибылью.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Когда обычно выходят прогнозы?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Прогнозы выходят обычно не позже чем за 3 часа до начала матча. Так что вы будете иметь возможность поставить ставку.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Куда обычно приходят прогнозы?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>На е-mail, который вы указали при регистрации.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Как я могу оплатить ваши прогнозы?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>У нас вы можете оплатить практически всеми доступными методами. Это банковская карта с поддержкой интернет платежей Visa/Mastercard, QIWI, Яндекс-деньги, Webmoney, Skrill и даже Paypal.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Какой нужен рекомендованный игровой банк?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Такой, чтобы вы спокойно себя чувствовали пока Пакет не закроется. Чем больше тем лучше. 10000 рублей вполне подходящий банк.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Какие конторы вы бы порекомендовали?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Лучшей конторой по праву называют Pinnacle Sports, очень достойные конкуренты bet365 и 1xbet.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Почему рекомендованная ставка равна 2000р?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Это сделано для того, чтобы у игрока была возможность закрыть Пакет и остаться в необходимом плюсе уже после первой ставки.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Что если коэффициент в моей конторе меньше чем тот, что дали вы?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Измените рекомендованную сумму ставки. Просто разделите нужную вам сумму выигрыша (а у нас это стандарт 2000р) на текущий коэффициент-1, и тогда получите рекомендованную сумму ставки.</p>
							<p>Например, мы дали кэф 2, а у вас 1,9. Тогда 2000/(1,9-1) = 2220 рублей.
 Желательно сразу же проставляться по вышедшему прогнозу или максимум за час до игры, пока не было объявления составов, после которого, движения коэффициентов будут напоминать американские горки. </p>
						</div>
					</div>
					<div class="response-block">
						<h3>Что если я не успел поставить ставку по купленному мною прогнозу?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Увы. Мы не можем следить за тем, чтобы все клиенты успевали поставить. Так что, в независимости от того, поставили ли вы или нет, обязательства, взятые нами, будут считаться выполненными, а прогноз - засчитан. Но по возможности, мы будем выставлять прогнозы по Пакету минимум за 3 часа до начала матча.</p>
						</div>
					</div>
					<div class="response-block">
						<h3>Сколько можно заработать на Joybetting?<i class="fa fa-angle-right" aria-hidden="true"></i></h3>
						<div class="answer">
							<p>Начнем с того, что средний процент проходимости наших прогнозов около 70. Конечно, каждый месяц может показать разные результаты, но как показала практика, в среднем прибыль наших игроков при серьезном подходе и сумме рекомендованной ставки в 2000р составляет около 10 - 15000 рублей в месяц. Но вам ничто не мешает ставить больше и получать большую прибыль соответственно.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<script src="js/jquery.matchHeight.js" type="text/javascript"></script>

<?php
  include('footer.php');
?>