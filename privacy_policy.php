<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('main_sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 privacy_policy_page">
				<h1>О нас</h1>
				<div class="privacy_policy_wrap">
					<p>Общим требованием для всех пользователей нашего сайта является понимание и уважение друг к другу и к Администрации. И во избежание неприятных и конфликтных ситуаций, ниже мы приводим правила и условия пользования нашим ресурсом, с которыми Вы ознакомлены и автоматически соглашаетесь выполнять, начав пользоваться нашими услугами.</p>
					<ul>
						<li>Пользоваться нашими услугами могут лишь лица достигшие 18 летия.</li>
						<li>При принятии решения Вы должны четко осознавать, что азартные игры и ставки на них сопряжены с риском и только Вы ответственны в полной мере за него.</li>
						<li>Регистрируясь на нашем ресурсе, Вы предоставляете точные почтовые и финансовые данные и отвечаете за их достоверность.</li>
						<li>Администрация не несет ответственности за работу вашей электронной почты или работу других систем, из-за которой могут происходить сбои в получении почтовых рассылок , а также любые изменения коэффициентов, произошедших после выхода рассылки.</li>
						<li>Администрация нашего сайта не дает 100% гарантии выполнения прогнозов, поэтому жалобы по поводу их расхождения приниматься не будут.</li>
						<li>Наш сайт не несет ответственности за появление форс-мажорных обстоятельств, связанных с перебоями работы электричества, компьютеров, интернета и других телекоммуникаций.</li>
						<li>Наш сервис не берет на себя ответственность за работу платежных переводов, а также за возможные сбои и задержки платежей банков и платежных систем.</li>
						<li>Пользователям нашего ресурса строго запрещается перепродавать наши вышедшие прогнозы.</li>
						<li>При размещении наших материалов на сторонних ресурсах, должна использоваться гиперссылка на наш сайт в качестве указателя источника этих материалов.</li>
						<li>Мы рассчитываем на взаимное уважение между нами - Администрацией сайта и вами - пользователями этого сайта. Поэтому, не допускаются хамство и оскорбления в чей-либо адрес.</li>
					</ul>
					<p>Невыполнение Правил и Условий использования нашего ресурса может привести к разрыву сотрудничества с Вами. Не доводите до такого, ведь наша общая цель - зарабатывать на ставках на спорт. И если вы серьезно отнесетесь к ней, у нас впереди могут быть годы плодотворного сотрудничества.</p>
					<p>Администрация Joybetting не занимается организацией ставок и играми на деньги. Вся информация носит ознакомительный характер и используется соответственно.</p>
				</div>				
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>