<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<h1>Личный кабинет</h1>
				<div class="my_account">
					<div class="row overall-statistic">
						<div class="col-sm-4 col-xs-12">
							<div class="stat-block">
								<span class="total-sum">330</span>
							<p>Всего пакетов</p>
							<ul class="data-block">
								<li>
									<div>
										<i class="fa fa-play-circle" aria-hidden="true"></i>
										<span>18</span>
									</div>
									<p>В игре</p>
								</li>
								<li>
									<div>
										<i class="fa fa-clock-o fa-clock-focus" aria-hidden="true"></i>
										<span>30</span>
									</div>
									<p>В ожидании</p>
								</li>
							</ul>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="stat-block">
								<span class="total-sum">178 733 &#8381;</span>
								<p>Выигрыш</p>
								<ul class="data-block">
									<li>
										<div>
											<i class="fa fa-check" aria-hidden="true"></i>
											<span>270</span>
										</div>
										<p>Выигрышных пакетов</p>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="stat-block">
								<span class="total-sum">10 456 &#8381;</span>
								<p>Проигрыш</p>
								<ul class="data-block">
									<li>
										<div>
											<i class="fa fa-times" aria-hidden="true"></i>
											<span>12</span>
										</div>
										<p>Проигрышных пакетов</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<h2>Баллы</h2>
					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="scoreboard">
								<div class="row">
									<div class="col-sm-6 col-xs-12 purse main">
										<div class="purse-wrap">
											<div class="purse-points">
												<img src="images/purse_points.png" alt="purse-points">
												<span>5 277</span>
											</div>
											<p>Всего бонусов</p>
											<strong>1 бонус = 1 рубль</strong>
										</div>
									</div>
									<div class="col-sm-6 col-xs-12 purse secondary">
										<div class="purse-wrap">
											<div class="purse-points">
												<img src="images/purse_points_plus.png" alt="purse-points">
												<span>2 570</span>
											</div>
											<p>Последние зачисления</p>
										</div>
										<div class="purse-wrap">
											<div class="purse-points">
												<img src="images/purse_points_minus.png" alt="purse-points">
												<span>1 213</span>
											</div>
											<p>Последние списания</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<h2>Счета</h2>
					<div class="row">
						<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
							<div class="my_scores_wrap">
								<table class="table my_scores">
									<tr>
										<td class="id_check item_id link-cell">Счет # <a href="#">000001</a></td>
										<td>07.12.2016</td>
										<td>3 220 р.</td>
										<td><a href="#" class="pay">Оплатить</a></td>
										<td><i class="fa fa-clock-o" aria-hidden="true"></i></td>
									</tr>
									<tr>
										<td class="id_check item_id link-cell">Счет # <a href="#">000012</a></td>
										<td>07.12.2016</td>
										<td>5 220 р.</td>
										<td><a href="#" class="used">Оплачен</a></td>
										<td><i class="fa fa-check-circle" aria-hidden="true"></i></td>
									</tr>
									<tr>
										<td class="id_check item_id link-cell">Счет # <a href="#">000033</a></td>
										<td>07.12.2016</td>
										<td>3 620 р.</td>
										<td><a href="#" class="used">Проверяем</a></td>
										<td><i class="fa fa-clock-o fa-clock-focus" aria-hidden="true"></i></td>
									</tr>
									<tr>
										<td class="id_check item_id link-cell">Счет # <a href="#">000041</a></td>
										<td>07.12.2016</td>
										<td>1 430 р.</td>
										<td><a href="#" class="used">Отменен</a></td>
										<td><i class="fa fa-minus-circle" aria-hidden="true"></i></td>
									</tr>
									<tr>
										<td class="id_check item_id link-cell">Счет # <a href="#">000161</a></td>
										<td>07.12.2016</td>
										<td>3 220 р.</td>
										<td><a href="#" class="used">Оплачен</a></td>
										<td><i class="fa fa-check-circle" aria-hidden="true"></i></td>
									</tr>
								</table>
							</div>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>