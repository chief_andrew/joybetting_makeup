<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block statistic-list">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('main_sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<h1>Статистика</h1>
				<div class="my_blocks_list">
					<div class="row blocks-list-wrap">
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-times" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-times" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-times" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
					</div>
					<nav aria-label="Page navigation">
					    <ul class="pagination">
							<li><span aria-hidden="true" class="disabled">Сюда</span></li>
							<li><span class="current">1</span></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#" aria-label="Next">Туда</a></li>
					    </ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</main>
<script src="js/jquery.matchHeight.js" type="text/javascript"></script>

<?php
  include('footer.php');
?>