<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></button>
				<h4 class="modal-title">Комментарий для ID:<span></span></h4>
			</div>
			<div class="modal-body">
				<p>Текст (содержимое) комментария</p>			
			</div>
		</div>
	</div>
</div>