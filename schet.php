<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<form action="" method="POST" class="schet-form">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left-panel panel_height">
							<ul class="container-fluid">
								<li>
									<h3>Первый пакет</h3>
									<p>Счет #<span>1000734</span></p>
								</li>
								<li>
									<div class="fa-block">
										<i class="fa fa-shopping-bag" aria-hidden="true"></i>
									</div>
									<div class="info-block">
										<p>Пакетов</p>
										<span>30</span>
									</div>
								</li>
								<li>
									<div class="fa-block">
										<i class="fa fa-credit-card" aria-hidden="true"></i>
									</div>
									<div class="info-block">
										<p>К оплате</p>
										<span>23 000</span><span>р.</span>
									</div>
								</li>
								<li>
									<div class="fa-block">
										<i class="fa fa-calendar" aria-hidden="true"></i>
									</div>
									<div class="info-block">
										<p>Дата</p>
										<span>07.12.2016</span>
									</div>
								</li>
								<li>
									<div class="fa-block">
										<i class="fa fa-gift" aria-hidden="true"></i>
									</div>
									<div class="info-block">
										<p>Баллов</p>
										<span>1 377</span>
									</div>
								</li>
							</ul>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 right-panel panel_height">
							<div class="stsus-row expectation">
								<span>Ожидаем оплаты</span>
								<i class="fa fa-clock-o" aria-hidden="true"></i>
							</div>
							<div class="container-fluid">
								<div class="form-group payment-method">
									<h4>Выберите способ оплаты</h4>
									<ul id="schet-payment-method">
										<li>
											<input id="Visa_MasterCard" type="radio" name="payment_method" value="Visa_MasterCard" checked>
											<label for="Visa_MasterCard">Visa/MasterCard</label>
										</li>
										<li>
											<input id="qiwi_money" type="radio" name="payment_method" value="qiwi_money">
											<label for="qiwi_money">QIWI</label>
										</li>
										<li>
											<input id="yandex_money" type="radio" name="payment_method" value="yandex_money">
											<label for="yandex_money">Яндекс Деньги</label>
										</li>
										<li>
											<input id="webmoney" type="radio" name="payment_method" value="webmoney">
											<label for="webmoney">Webmoney</label>
										</li>
										<li>
											<input id="pay_pal" type="radio" name="payment_method" value="pay_pal">
											<label for="pay_pal">PayPal</label>
										</li>
										<li>
											<input id="scrill" type="radio" name="payment_method" value="scrill">
											<label for="scrill">Scrill</label>
										</li>
									</ul>
								</div>
								<div class="info-message">
									<p>Для оплаты счета перейдите по <a href="#">ссылке</a>. На странице платежной системы заполните форму. Введите сумму <span>1000</span><strong>р</strong>, в поле комментарий напишите ваш email, выберите оплату с банковской карты, введите данные карты и нажмите на ка кнопку перевести.</p>
								</div>
								<div class="form-group">
									<p>Для ускорения процесса идентификации перевода, после оплаты счета отправьте реквизиты оплаты.</p>
								</div>
								<div class="form-group">
									<textarea name="comments" class="form-control" rows="2">Введите сюда реквизиты оплаты и нажмите кнопку отправить.</textarea>
								</div>
								<div class="form-group">
									<button class="btn btn-primary btn-lg" type="submit">Отправить</button>
								</div>
							</div>							
						</div>
					</div>					
				</form>				
			</div>
		</div>
	</div>
</main>
<script src="js/buy_unit.js" type="text/javascript"></script>
<?php
  include('footer.php');
?>