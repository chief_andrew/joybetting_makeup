<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-2 col-sm-3 sidebar-panel">
				<?php include('sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-10 col-sm-9 col-xs-12">
				<h1>Мои пакеты</h1>
				<div class="my_blocks_list">
					<div class="bottom_line navbar-collapse">
						<ul class="statistic-row">
							<li>Всего:<span>73</span></li>
							<li><i class="fa fa_created" aria-hidden="true"></i>В ожидании:<span>2</span></li>
							<li><i class="fa fa-play-circle" aria-hidden="true"></i>В игре:<span>2</span></li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Выигрышных:<span>270</span></li>
							<li><i class="fa fa-times-circle" aria-hidden="true"></i>Проигрышных:<span>21</span></li>
						</ul>
					</div>
					<div class="row blocks-list-wrap">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item in_game add_in_game">
								<span class="sticker"></span>
								<div class="container-fluid">
									<h3>Добавить пакет в игру</h3>
									<p>Вы можете одновременно играть в нескольких пакетах. Для этого просто нажмите на кнопку добавить</p>
								</div>
								<a href="#" class="btn"><i class="fa fa-plus" aria-hidden="true"></i>Добавить</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item in_game add_in_game">
								<span class="sticker"></span>
								<div class="container-fluid">
									<h3>Добавить пакет в игру</h3>
									<p>Вы можете одновременно играть в нескольких пакетах. Для этого просто нажмите на кнопку добавить</p>
									<select class="form-control">
									    <option>live</option>
									    <option>Первый</option>
									</select>
								</div>
								<a href="#" class="btn"><i class="fa fa-plus" aria-hidden="true"></i>Добавить</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item in_game">
								<span class="sticker">Live</span>
								<div class="container-fluid">
									<div class="circle-wrap">
										<div class="circle-fa-wrap">
											<i class="fa fa-play-circle" aria-hidden="true"></i>
										</div>
										<a href="#">Первый #1783000</a>
									</div>
								</div>
								<a href="#" class="btn">Играть</a>
							</div>			
						</div>
						
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item in_game">
								<span class="sticker"></span>
								<div class="container-fluid">
									<div class="circle-wrap">
										<div class="circle-fa-wrap">
											<i class="fa fa-play-circle" aria-hidden="true"></i>
										</div>
										<a href="#">Первый #1783000</a>
										<p>Играет:</p>
										<span>Йонг Аякс Йонг Аякс - Алмере Сити Алмере Сити</span>
									</div>
								</div>
								<a href="#" class="btn">Играть</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item in_game">
								<span class="sticker"></span>
								<div class="container-fluid">
									<div class="circle-wrap">
										<div class="circle-fa-wrap">
											<i class="fa fa-play-circle" aria-hidden="true"></i>
										</div>
										<a href="#">Первый #1783000</a>
									</div>
								</div>
								<a href="#" class="btn">Играть</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-times" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-times" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="block-item">
								<div class="container-fluid">
									<div class="block-head">
										<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
									</div>
									<p>Пакет закрыт в плюсе</p>
									<div class="win_sum">17 870 ₽</div>
									<p>Сыграно 5 ставок</p>
									<ul>
										<li><span>9.7</span>cуммарный коэффициент</li>
										<li><span>85</span>процент проходимости</li>
										<li><span>2 385</span>средняя сумма ставки</li>
										<li><span>27</span>выкуплено блоков</li>
									</ul>								
								</div>
								<a href="#" class="btn">Подробнее</a>
							</div>			
						</div>
					</div>
					<nav aria-label="Page navigation">
					    <ul class="pagination">
							<li><span aria-hidden="true" class="disabled">Сюда</span></li>
							<li><span class="current">1</span></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#" aria-label="Next">Туда</a></li>
					    </ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>