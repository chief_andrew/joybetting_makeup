<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form action="" mathod="POST" id="log-in" class="login-form">
					<h1>Регистрация</h1>
					<div class="form-group">
						<label for="exampleInputName2">Имя</label>
						<input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter yor name">
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1">Адрес электронной почты</label>
						<input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Придумайте пароль</label>
						<input type="password" class="form-control" id="user_pass1" name="user_password1" placeholder="Password">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword2">Повторите, чтобы не ошибиться</label>
						<input type="password" class="form-control" id="user_pass2" name="user_password2" placeholder="Password">
					</div>
					<div class="our-rules">
						<p>Создавая аккаунт, вы соглашаетесь с нашими <a href="#">Правилами и условиями</a> и
						 <a href="#">Положением о конфиденциальности</a></p>
					</div>
					<div class="submit-box">
						<div class="row">
							<div class="col-md-6 submit-wrap">
								<button type="submit" class="btn btn-primary btn-lg btn-block">Войти</button>
							</div>
						</div>
					</div>
					<div class="create-account-link">
						<span>Уже есть аккаунт?</span>
						<a href="login.php">Войдите</a>
					</div>	
				</form>
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>