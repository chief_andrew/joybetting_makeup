<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block statistic-list">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('main_sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<h1>Отзывы (<span>73 277</span>)</h1>
				<div class="reviews-container">
					<form class="form-notice container-fluid">
						<div class="row">
							<div class="form-group col-xs-12">
								<textarea name="notaice_massege" id="notaice_massege" rows="4"></textarea>
							</div>
							<div class="form-group col-md-4 col-sm-6 col-xs-12">
								<button class="btn btn-primary btn-lg btn-block" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Отправить</button>
							</div>
						</div>
					</form>
					<div class="reviews">
						<div class="rev_item">
							<h3>Roy Schlegel</h3>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus pulvinar tempor.
						</div>
						<div class="rev_item">
							<h3>Fork</h3>
							Его смысл - это вручную скопировать код пакета себе в проект и прописать автозагрузку. Только копировать проект стоит не в папку vendor а в свою произвольную папку, например, lib. И автозагрузку прописывать не меняя внутренние файлы composer'а, а добавив информацию в специальную секцию autoload конфигурации А что именно там прописать подскажет сам пакет. Заходим в его исходный код и смотрим в его composer.json - там так же есть секция autoload, вот её и повторяем, только добавив в начало пути, естественно, нашу часть, куда мы разместили пакет (т.е. например lib/ИмяПакета). И, вообще, желательно прочитать документацию по автозагрузке в php, о стандартах .
						</div>
						<div class="rev_item">
							<h3>ThunderPuff</h3>
							Сервисы Google для социального взаимодействия и обмена данными позволяют разным людям общаться, делиться опытом, совместно работать над проектами и формировать новые сообщества. А наши правила помогают создать комфортные условия для всех пользователей перечисленных ниже сервисов (далее совместно именуемых Службами). Соблюдайте их.Для поиска материалов, нарушающих наши правила, нам крайне необходима помощь пользователей. Узнав о нарушении правил, мы можем принять необходимые меры, в том числе ограничить доступ к контенту, удалить его или ограничить доступ пользователя к продуктам Google. Обратите внимание, что мы можем делать исключения из действующих правил для материалов, представляющих художественную, образовательную, историческую либо иную ценность для общества.
						</div>
						<div class="rev_item">
							<h3>Colleen O'Neil</h3>“while I just tried to stand up straight and not to make any unintentionally weird faces.” I just hate it when my facial muscles run amok, I know just what you mean. Another hilarious piece, thank you. That’s a great drawing of the hot guy, by the way.
						</div>
						<div class="rev_item">
							<h3>Lei Mohanachandran</h3>
							Oh, I’m certain my recollection of Armageddon is somewhat clouded by the less than ideal circumstances under which I saw it. Beyond any scientific inaccuracies, I think my main objection really boils down to the fact that someone thought it was ever appropriate to include Sweet Emotion on the same soundtrack as THE SONG THAT SHALL NOT BE NAMED (to be clear it’s, I Don’t Wanna Miss a Thing). I know as a woman I’m ‘sposed to like the love song stuff, but, seriously, don’t piss in my ears and tell me it’s raining.
						</div>
						<div class="rev_item">
							<h3>Gray</h3>
							Thank you, I especially needed to read this today. I’ve spent so much time lately focusing on what I don’t have that I haven’t remembered to be grateful for all that I do have.
						</div>
						<div class="rev_item">
							<h3>Alford Wayman</h3>
							I’m rather pessimistic and belive the Universe holds me no more special then then anything else among the living. To me saying thank-you only feeds into the illusion that it might. When I wake up every morning I think “man did I get lucky this time.” I’m not sure why I need to thank the abuser, by fate or chance, for not harming me, or for the things I enjoy. This alone makes those things all the more enjoyable because it maybe you last. A person never knows when they will be slapped on the back by fates open hand, and fall head into the dust, never to stand again. We are all like Job or Patroculus, simply waiting our turns. I sure hope I am lucky.
						</div>
						<div class="rev_item">
							<h3>McLaughlin</h3>
							Ahh yes ! I too have written about how art even existed, i will be posting it soon but couldn’t agree more with the above statement you have wrote. Its amazing i can run into same alike minds, I THANK YOU for your piece of mind in which is like an art manifesting and enduring our sight, fulfilling the thrills of our ideas ! I love it, Thank youuuu.
						</div>
					</div>
				</div>
				<nav aria-label="Page navigation">
					<ul class="pagination">
						<li><span aria-hidden="true" class="disabled">Сюда</span></li>
						<li><span class="current">1</span></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#" aria-label="Next">Туда</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</main>
<script src="js/jquery.matchHeight.js" type="text/javascript"></script>

<?php
  include('footer.php');
?>