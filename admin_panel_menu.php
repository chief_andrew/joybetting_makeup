<main id="wrapper">
    <nav class="navbar navbar-default" id="nav_md_menu">
        <div class="container">
            <div class="bottom_line navbar-collapse">
                <ul class="navbar-right nav_md_right statistic-row">
                    <li class="percent">87%</li>
                    <li class="quantity_projected">7777</li>
                    <li><i class="fa fa-check-circle" aria-hidden="true"></i>7000</li>
                    <li><i class="fa fa-times-circle" aria-hidden="true"></i>70</li>
                    <li><i class="fa fa-dot-circle-o" aria-hidden="true"></i>707</li>
                    <li><i class="fa fa-exclamation-circle" aria-hidden="true"></i>7</li>
                    <li><i class="fa fa-play-circle" aria-hidden="true"></i>707</li>
                </ul>
                <ul class="nav navbar-nav navbar-left nav_md_left">
                    <li><a href="admin_panel_prognoses.php">Прогнозы</a></li>
                    <li><a href="admin_panel_add_prognoses.php">Создать</a></li> 
                    <li><a href="admin_panel_buying_prognoses.php">Счета</a></li>
                    <li><a href="admin_panel_clients.php">Клиенты</a></li>
                </ul>    
            </div>
        </div>
    </nav>
	<script type="text/javascript">
		$(function () { 
			var location = window.location.href;		 
			var cur_url = location.split('/').pop();		 
			$('.nav_md_left li>a').each(function () {		 
				var link = $(this).attr('href');	
				if(cur_url == link) $(this).addClass('current');		 
			});		 
		});
	</script>