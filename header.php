<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Bootstrap 101 Template</title>

        <!-- Bootstrap -->    
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Bootstrap-datetimepicker --> 
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
        <!-- Custom styles -->
        <link href="css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- connect script jQuery -->
        <script type="text/javascript" src="js/jquery.js"></script>
    </head>
    <body>
        <header id="header">
           <div class="blind-wrap">
				<div class="blind">
					<div class="container">
						<div class="row">
							<div class="col-sm-7 description-box">
								<h1>Что такое Joybetting</h1>
								<p>Сервисы Google для социального взаимодействия и обмена данными позволяют разным людям общаться, делиться опытом, совместно работать над проектами и формировать новые сообщества. А наши правила помогают создать комфортные условия для всех пользователей.</p>
								<p>Для поиска материалов, нарушающих наши правила, нам крайне необходима помощь пользователей. Узнав о нарушении правил, мы можем принять необходимые меры, в том числе.</p>
								<p>Обратите внимание, что мы можем делать исключения из действующих правил для материалов, представляющих художественную.</p>
							</div>
							<div class="col-sm-5 hidden-xs image-box">
								<img src="images/logo_full_icon.png" alt="" class="img-responsive">
							</div>
						</div>						
					</div>					
				</div>
				<div class="blind-arrow">
					<div class="down"><i class="fa fa-angle-down" aria-hidden="true"></i>Что такое Joybetting?</div>
					<div class="up"><i class="fa fa-angle-up" aria-hidden="true"></i></div>
				</div>
            </div>
            <nav class="navbar navbar-default" id="header_menu">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="images/logo.png" alt="Logotype" class="img-responsive">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">            
                        <ul class="nav navbar-nav navbar-right-second">
                            <li><a href="login.php">Вход</a></li>
                            <li><a href="registration.php">Регистрация</a></li>            
                        </ul>         
                        <ul class="nav navbar-nav navbar-right navbar-right-first">
                            <li><a href="#">Прайс</a></li>
                            <li><a href="#">Статистика</a></li>
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Помощь</a></li>
                        </ul>           
                    </div>
                </div>
            </nav>  
        </header>