<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="page_not_found">
			<div class="p404">
				404
			</div>
			<p>Такой страницы нет</p>
			<a href="#">На главную</a>
		</div>
	</div>
</main>
<script src="js/jquery.matchHeight.js" type="text/javascript"></script>

<?php
  include('footer.php');
?>