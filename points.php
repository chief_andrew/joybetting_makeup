<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 points-page">
				<h1>Мои Бонусы</h1>
				<div class="row score-wrap">
					<div class="col-sm-4 scoreboard">
						<ul class="container-fluid">
								<li>
									<div class="purse-wrap">
										<div class="purse-points">
											<img src="images/purse_points.png" alt="purse-points">
											<span>5 277</span>
										</div>
										<p>Всего бонусов</p>
									</div>
								</li>
								<li>
									<div class="purse-wrap">
										<div class="purse-points">
											<img src="images/purse_points_plus.png" alt="purse-points">
											<span>2 570</span>
										</div>
										<p>Последние зачисления</p>
									</div>
								</li>
								<li>
									<div class="purse-wrap">
										<div class="purse-points">
											<img src="images/purse_points_minus.png" alt="purse-points">
											<span>1 213</span>
										</div>
										<p>Последние списания</p>
									</div>
								</li>
								<li>
									<div class="purse-wrap">
										1 бонус = 1 рубль
									</div>
								</li>
							</ul>
					</div>
					<div class="col-sm-8 scoreboard-rules">
						<h4>Что такое Бонусы и как ими воспользоваться?</h4>
						<p>Бонусы начисляются за каждую покупку, независимо от суммы! Количество бонусов указывается во время покупки. С их помощью можно оплатить будущие приобретения, но не больше 30% стоимости услуги.</p>
						<h4>Какой срок действия бонусов?</h4>
						<p>Срок действия бонусов составляет 7 месяцев с момента их начисления. По истечении указанного времени неиспользованные бонусы аннулируются :-(. Поэтому не забудь их потратить в срок. </p>
						<h4>Когда Бонусы появятся на моем счете?</h4>
						<p>Моментально после совершения покупки.</p>
					</div>
				</div>
				<h3>История</h3>
				<table class="table points-score-table">
					<thead>
						<tr>
							<th class="id_check">Дата</th>
							<th>Комментарий</th>
							<th>Сумма баллов</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="id_check">07.12.2016</td>
							<td>Списаны за покупку пакета -(ов)</td>
							<td class="sum_of_points">-1000</td>
						</tr>
						<tr>
							<td class="id_check">07.12.2016</td>
							<td>Начислены за покупку пакета -(ов)</td>
							<td class="sum_of_points">+3920</td>
						</tr>
						<tr>
							<td class="id_check">07.12.2016</td>
							<td>Специальные бонусы</td>
							<td class="sum_of_points">+1000</td>
						</tr>
						<tr>
							<td class="id_check">07.12.2016</td>
							<td>Начислены за покупку пакета -(ов)</td>
							<td class="sum_of_points">+3220</td>
						</tr>
						<tr>
							<td class="id_check">07.12.2016</td>
							<td>Специальные бонусы</td>
							<td class="sum_of_points">-1000</td>
						</tr>
						<tr>
							<td class="id_check">07.12.2016</td>
							<td>Истек срок действия бонусов</td>
							<td class="sum_of_points">-7300</td>
						</tr>
						<tr>
							<td class="id_check">07.12.2016</td>
							<td>Списаны за покупку пакета -(ов)</td>
							<td class="sum_of_points">-800</td>
						</tr>
					</tbody>
				</table>
				<div class="container-fluid statistics-row">
					<div class="statistics-string">						
						<ul class="pager nextprev">
							<li class="disabled">
								<span><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></span>
							</li>
							<li>
								<a href="#" rel="next"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>