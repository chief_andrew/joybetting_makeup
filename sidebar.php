<nav class="navbar navbar-default">
	<ul class="account-navmenu nav">
		<li><a href="my_account.php">Мой кабинет</a></li>
		<li><a href="buy_unit.php">Купить пакет</a></li>
		<li><a href="history.php">Моя история</a></li>
		<li><a href="blocks.php">Мои пакеты</a></li>
		<li><a href="points.php">Мои баллы</a></li>
		<li><a href="setting.php">Мои настройки</a></li>
	</ul>
</nav>
<script type="text/javascript">
	$(function () { 
		var location = window.location.href;		 
		var cur_url = location.split('/').pop();		 
		$('.account-navmenu li>a').each(function () {		 
			var link = $(this).attr('href');	
			if(cur_url == link) $(this).addClass('current');		 
		});		 
	});
</script>