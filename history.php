<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<h1>Моя история</h1>
				<div class="history_table_block">
					<table class="table table-striped table-bordered responsive">
						<thead>
							<tr>
								<th class="id_check">#</th>
								<th>Дата</th>
								<th>Вид</th>
								<th>Баллов</th>
								<th>Пакетов</th>
								<th>К оплате</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="id_check"><a href="">000001</a></td>
								<td>07.12.2016</td>
								<td>Стандарт</td>
								<td>100</td>
								<td>1</td>
								<td>1000 р.</td>
								<td class="fa-cell"><i class="fa fa-clock-o" aria-hidden="true"></i></td>
							</tr>
							<tr>
								<td class="id_check"><a href="">000002</a></td>
								<td>07.12.2016</td>
								<td>Стандарт</td>
								<td>100</td>
								<td>1</td>
								<td>1000 р.</td>
								<td class="fa-cell"><i class="fa fa-check-circle" aria-hidden="true"></i></td>
							</tr>
							<tr>
								<td class="id_check"><a href="">000003</a></td>
								<td>07.12.2016</td>
								<td>Стандарт</td>
								<td>100</td>
								<td>1</td>
								<td>1000 р.</td>
								<td class="fa-cell"><i class="fa fa-minus-circle" aria-hidden="true"></i></td>
							</tr>
							<tr>
								<td class="id_check"><a href="">000004</a></td>
								<td>07.12.2016</td>
								<td>Стандарт</td>
								<td>100</td>
								<td>1</td>
								<td>1000 р.</td>
								<td class="fa-cell"><i class="fa fa-clock-o" aria-hidden="true"></i></td>
							</tr>                            
						</tbody>
					</table>
					<div class="container-fluid statistics-row">
						<div class="statistics-string">
							<div class="counter font-bold">7 777 заявок</div>
							<ul class="pager nextprev">
								<li class="disabled">
									<span><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></span>
								</li>
								<li>
									<a href="#" rel="next"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>