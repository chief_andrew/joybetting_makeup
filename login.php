<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form action="" mathod="POST" id="log-in" class="login-form">
					<h1>Вход</h1>
					<div class="form-group">
						<label for="exampleInputEmail1">Адрес электронной почты</label>
						<input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Пароль для  Joybetting</label>
						<input type="password" class="form-control" id="user_pass1" name="user_password1" placeholder="Password">
					</div>
					<div class="checkbox">
						<label for="remember_me">
							<label>
							    <input type="checkbox" id="remember_me">
							    <span class="checkbox-custom">&#10004;</span>
							</label>
							<span class="rem_me">Запомнить меня</span>
						</label>
					</div>
					<div class="submit-box">
						<div class="row">
							<div class="col-md-6 submit-wrap">
								<button type="submit" class="btn btn-primary btn-lg btn-block">Войти</button>
							</div>
							<div class="col-md-6">
								<a href="forgot-password.php">Забыли пароль?</a>
							</div>
						</div>
					</div>
					<div class="create-account-link">
						<span>Еще нет аккаунта?</span>
						<a href="registration.php">Создайте его</a>
					</div>	
				</form>
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>