<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<h1>Купить пакет</h1>
				<form action="" method="POST" class="buy_package">
					<div class="row pack-head">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 type-pack-head">
							<input id="standart-package" type="radio" name="type_package" value="standart" checked>
							<label for="standart-package">
								<h3>Первый пакет</h3>
							</label>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 type-pack-head">
							<input id="live-package" type="radio" name="type_package" value="live">
							<label for="live-package">
								<h3>Live пакет</h3>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-9 col-xs-12 pack_block1">
							<div class="type-package type-pack-block1">
								<output for="type_package" class="form-group output1"></output>
								<div class="form-group">
									<h4>Выберите количество пакетов</h4>
									<select name="select_packages" class="form-control input-lg">
									</select>
								</div>
								<div class="form-group payment-method">
									<h4>Выберите способ оплаты</h4>
									<ul>
										<li>
											<input id="Visa_MasterCard" type="radio" name="payment_method" value="Visa_MasterCard" checked>
											<label for="Visa_MasterCard">Visa/MasterCard</label>
										</li>
										<li>
											<input id="qiwi_money" type="radio" name="payment_method" value="qiwi_money">
											<label for="qiwi_money">QIWI</label>
										</li>
										<li>
											<input id="yandex_money" type="radio" name="payment_method" value="yandex_money">
											<label for="yandex_money">Яндекс Деньги</label>
										</li>
										<li>
											<input id="webmoney" type="radio" name="payment_method" value="webmoney">
											<label for="webmoney">Webmoney</label>
										</li>
										<li>
											<input id="pay_pal" type="radio" name="payment_method" value="pay_pal">
											<label for="pay_pal">PayPal</label>
										</li>
										<li>
											<input id="scrill" type="radio" name="payment_method" value="scrill">
											<label for="scrill">Scrill</label>
										</li>
									</ul>
								</div>
								<div class="with_bonus form-group">
									<h4>Оплата бонусами</h4>
									<p>На счету <b>15</b> бонусов</p>
									<div id="show_bonus_withdrawal">0</div>
									<label><input type="checkbox" name="with_bonus">Списать бонусы</label>
								</div>
								<div class="form-group payment-outputs">
									<output name="old_price" class="old_price"><span></span><i></i> &#8381;</output>
									<output name="current_price" class="current_price"><i></i> <span>&#8381;</span></output>
								</div>
								<div class="form-group bonus-string">
									<div class="fa_block">
									</div>
									<div class="text_block">
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Купить</button>
								</div>
								<div class="form-group">
									<p>Во время игры следуйте нашим рекомендациям. Они специально разработаны для того чтобы Вы, всегда были в плюсе!</p>
								</div>											
							</div>
						</div>
						<div class="col-lg-6 col-md-6 pack_block2">
							<div class="type-package type-pack-block2">
								<ul class="list_of_benefits">
									<li class="row">
										<div class="col-md-2">
											<i class="fa fa-thumbs-up" aria-hidden="true"></i>
										</div>
										<div class="col-md-10">
											<h3>Предложения на любой бюджет!</h3>
											<p>Выгодные предложения для новичков и игроков со стажем.</p>
										</div>
									</li>
									<li class="row">
										<div class="col-md-2">
											<i class="fa fa-play-circle" aria-hidden="true"></i>
										</div>
										<div class="col-md-10">
											<h3>Игра продолжается</h3>
											<p>Покупая блок вы будите получать прогнозы до тех пор пока не удвоите свой банк! только после этого блок закроется.</p>
										</div>
									</li>
									<li class="row">
										<div class="col-md-2">
											<i class="fa fa-life-ring" aria-hidden="true"></i>
										</div>
										<div class="col-md-10">
											<h3>Поддержка пользователей</h3>
											<p>Мы готовы поддержать каждого пользователя.</p>
										</div>
									</li>
									<li class="row">
										<div class="col-md-2">
											<i class="fa fa-certificate" aria-hidden="true"></i>
										</div>
										<div class="col-md-10">
											<h3>Опыт аналитиков</h3>
											<p>Прогнозы даются только экспертами с многолетним стажем с проходимостью не менее 77%.</p>
										</div>
									</li>
									<li class="row">
										<div class="col-md-2">
											<i class="fa fa-gift" aria-hidden="true"></i>
										</div>
										<div class="col-md-10">
											<h3>Зарабатывайте бонусы</h3>
											<p>Получайте бонусы за покупку блоков, привлечение пользователей и активное участие в проекте.</p>
										</div>
									</li>
								</ul>
							</div>
						</div>						
					</div>
				</form>				
			</div>
		</div>
	</div>
</main>
<script src="js/buy_unit.js" type="text/javascript"></script>
<?php
  include('footer.php');
?>