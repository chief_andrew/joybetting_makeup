<nav class="navbar navbar-default">
	<ul class="account-navmenu  nav">
		<li><a href="price.php">Цены</a></li>
		<li><a href="about_us.php">О нас</a></li>
		<li><a href="help.php">Помощь</a></li>
		<li><a href="statistics.php">Статистика</a></li>
		<li><a href="points2.php">Баллы</a></li>
		<li><a href="paiment.php">Оплата</a></li>
		<li><a href="reviews.php">Отзывы</a></li>
		<li><a href="privacy_policy.php">Правила и условия</a></li>
	</ul>
</nav>
<script type="text/javascript">
	$(function () { 
		var location = window.location.href;		 
		var cur_url = location.split('/').pop();		 
		$('.account-navmenu li>a').each(function () {		 
			var link = $(this).attr('href');	
			if(cur_url == link) $(this).addClass('active');		 
		});		 
	});
</script>