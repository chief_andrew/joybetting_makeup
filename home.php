<?php
  include('header3.php');
?>

<main id="wrapper">
	<div class="top-home-banner home-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-xs-12 banner-text-block">
					<div class="banner-text">
						<h1>Сервис спортивной аналитики</h1>
						<h3>Играйте с удовольствием вместе с Joybetting</h3>
						<span class="separate-line"></span>
						<div class="row">
							<acrticle class="col-md-9 col-sm-12 col-xs-12">Проект создан специально для тех, кто любит спортивные ставки, азарт и хочет быть в плюсе. Вы готовы зарегистрироваться и начать получать прибыль? Нажмите на кнопку регистрации, чтобы приступить к работе!</acrticle>
						</div>
						<div class="row button-wrap">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<a href="#" class="btn btn-primary">Начать работу</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12 window-block">
					<div class="window-wrap">
						<div class="window-top-line">
							<span class="circle-red"></span>
							<span class="circle-yellow"></span>
							<span class="circle-green"></span>
						</div>
						<img src="images/home-banner.png" alt="home-banner" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container content-preview home-page">
		<h1>Joybetting — любовь с первого клика</h1>
		<div class="row articles-block">
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="img-block">
					<img src="images/sport_analitic.png" alt="">
				</div>
				<h3>Спортивная аналитика</h3>
				<article>Прогнозы на футбол, баскетбол, хоккей, теннис и другие виды спорта от лучших экспертов.</article>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="img-block">
					<img src="images/star_trast.png" alt="">
				</div>
				<h3>Высокий уровень доверия</h3>
				<article>Стабильная работа сервиса, уровень дохода и качество предоставления услуг - все это повышает уровень доверия клиентов к Joybetting.</article>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="img-block">
					<img src="images/sun_statistics.png" alt="">
				</div>
				<h3>Прозрачная старистика</h3>
				<article>Специально для Вас, мы храним всю статистику прошлых событий начиная с первого дня работы сервиса. Не верите? Проверьте сами.</article>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="img-block">
					<img src="images/play_avery_time.png" alt="">
				</div>
				<h3>Играйте в удобное время</h3>
				<article>Мы побеспокоились, что бы у Вас был всегда выбор. Выбирайте время и собитие удобное для Вас.</article>
			</div>
		</div>	
		<div class="row articles-block">	
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="img-block">
					<img src="images/cup.png" alt="">
				</div>
				<h3>Заработок с удовольствием</h3>
				<article>Становитесь нашими постоянными пользователями и зарабатывайте, наслаждаясь игрой любимых спортивных команд.</article>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="img-block">
					<img src="images/checked_circle.png" alt="">
				</div>
				<h3>Цены для каждого</h3>
				<article>Одно из наших достоинств – это цена. Мы специально разработали гибкую систему цен, чтобы удовлетворить каждого клиента.</article>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="img-block">
					<img src="images/help_for_users.png" alt="">
				</div>
				<h3>Помощь пользователям</h3>
				<article>Быстрая и качественная поддержка повышает уровень доверия и располагает пользователей нашего сервиса к повторным покупкам.</article>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="img-block">
					<img src="images/squer_bag.png" alt="">
				</div>
				<h3>Бонусная программа</h3>
				<article>Возвращаем до 30% от суммы пакетов на бонусный счет. Экономьте используя бонусы при следующих покупках.</article>
			</div>
		</div>
	</div>
	<div class="how_do_we_get_results full-width home-page">
		<div class="container">
			<h1>Как мы получаем мощные результаты?</h1>
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12  header-description">
					<span>Чтобы убедиться в эффективности деятельности нашей команды и понять причины мощных результатов, достаточно посмотреть на схему нашей работы:</span>
				</div>
			</div>			
			<ul>
				<li>
					<div class="row">
						<div class="item-num col-sm-1 col-xs-3">01</div>
						<div class="description-block col-sm-11 col-xs-9">
							<h3>Сбор данных</h3>
							<article class="description">
								Каждый член команды собирает матчи, подходящие под прибыльные прогнозы, именно из той лиги, в которой специализируется. Затем собираются новости, статистика, травмы и прочая информация, касающееся выбранных матчей.
							</article>
						</div>
					</div>					
				</li>
				<li>
					<div class="row">
						<div class="item-num col-sm-1 col-xs-3">02</div>
						<div class="description-block col-sm-11 col-xs-9">
							<h3>Поверхностный анализ</h3>
							<article class="description">
								Каждый член команды собирает матчи, подходящие под прибыльные прогнозы, именно из той лиги, в которой специализируется. Затем собираются новости, статистика, травмы и прочая информация, касающееся выбранных матчей.
							</article>
						</div>
					</div>					
				</li>
				<li>
					<div class="row">
						<div class="item-num col-sm-1 col-xs-3">03</div>
						<div class="description-block col-sm-11 col-xs-9">
							<h3>Обсуждение и глубокий анализ</h3>
							<article class="description">
								Каждый член команды собирает матчи, подходящие под прибыльные прогнозы, именно из той лиги, в которой специализируется. Затем собираются новости, статистика, травмы и прочая информация, касающееся выбранных матчей.
							</article>
						</div>
					</div>					
				</li>
				<li>
					<div class="row">
						<div class="item-num col-sm-1 col-xs-3">04</div>
						<div class="description-block col-sm-11 col-xs-9">
							<h3>Финальный прогноз</h3>
							<article class="description">
								Каждый член команды собирает матчи, подходящие под прибыльные прогнозы, именно из той лиги, в которой специализируется. Затем собираются новости, статистика, травмы и прочая информация, касающееся выбранных матчей.
							</article>
						</div>
					</div>					
				</li>
				<li>
					<div class="row">
						<div class="item-num col-sm-1 col-xs-3">05</div>
						<div class="description-block col-sm-11 col-xs-9">
							<h3>Итоговый результат</h3>
							<article class="description">
								Каждый член команды собирает матчи, подходящие под прибыльные прогнозы, именно из той лиги, в которой специализируется. Затем собираются новости, статистика, травмы и прочая информация, касающееся выбранных матчей.
							</article>
						</div>
					</div>					
				</li>
			</ul>
		</div>
	</div>
	<div class="how_to_get_started home-page container">
		<h1>Как начать работу?</h1>
		<div class="row short-instruction-block">
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="circle-block">
					<div class="circle">
						<i>1</i>
					</div>
				</div>
				<span>Зарегистрируйтесь в нашей системе, это займет не более 30 секунд.</span>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="circle-block">
					<div class="circle">
						<i>2</i>
					</div>
				</div>
				<span>В личном кабинете выберите необходимый тип пакета и приобретите нужное их Вам количество.</span>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="circle-block">
					<div class="circle">
						<i>3</i>
					</div>
				</div>
				<span>Следуя подсказкам оплатите покупку любым удобным для Вас способом.</span>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="circle-block">
					<div class="circle">
						<i>4</i>
					</div>
				</div>
				<span>Joybetting — превосходный инструмент за разумные деньги. Проверьте сами</span>
			</div>
		</div>
		<div class="row button-row">
			<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 ">
				<a href="" class="btn btn-primary">Начать работу</a>
			</div>
		</div>	
	</div>
	<div class="affordable-service full-width home-page">
		<div class="container">
			<h1>Сервис, который вам по карману</h1>
			<div class="row">
				<div class="col-md-9 col-md-offset-1-5 col-sm-12 col-xs-12 header-description">
					<span>Joybetting — превосходный инструмент за разумные деньги. Проверьте сами</span>
				</div>
			</div>
			<div class="row service-row">
				<div class="col-md-9 col-md-offset-1-5 col-sm-12 col-xs-12">
					<div class="row service-block-wrap">
						<div class="col-md-6 col-sm-6 col-xs-12 servise-list">
							<ul>
								<li>
									<div class="service-item">
										<h3>Играем до полной победы</h3>
										<article>Вы продолжаете получать прогнозы до тех пор, пока не получите прибыль 2 000 р.</article>
									</div>
								</li>
								<li>
									<div class="service-item">
										<h3>Играем в удобное время</h3>
										<article>Выбирайте удобное для себя время или интересующие Вас события и делайте ставку.</article>
									</div>
								</li>
								<li>
									<div class="service-item">
										<h3>Играйте больше</h3>
										<article>У Вас есть возможность активировать любое количество пакетов, ставить больше и зарабатывать больше.</article>
									</div>
								</li>
							</ul>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 service-info">
							<h1>Пакет "Первый"</h1>
							<h3>Пакет рассчитан на стабильную прибыль</h3>
							<div class="sum-packege">
								<div class="sum-packege-control">
									<span class="minus"></span>
									<span class="count">31</span>
									<span class="plus"></span>
								</div>
								<p>Количество пакетов</p>
							</div>
							<div class="package-cost">
								<div class="price-block">
									<span class="price full_price"></span>
									<p>Стоимость пактов</p>
								</div>
							</div>
							<div class="package-cost bonus">
								<div class="price-block">
									<span class="price bonuses"></span>
									<p>Возвращаем на Ваш бонусный счет</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 col-md-offset-2">
									<a href="#" class="btn btn-primary">Начать работу</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="packege-statistic home-page">
		<div class="container">
			<h1>Статистика пакетов</h1>
			<div class="row">
				<div class="col-md-10 col-md-offset-1 header-description">
					<span>Прозрачная статистика, доступна абсолютно всем пользователям с момента создания первого пакета.</span>
				</div>
			</div>
			<div class="row blocks-list-wrap">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="block-item">
						<div class="container-fluid">
							<div class="block-head">
								<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
							</div>
							<p>Пакет закрыт в плюсе</p>
							<div class="win_sum">17 870 ₽</div>
							<p>Сыграно 5 ставок</p>
							<ul>
								<li><span>9.7</span>cуммарный коэффициент</li>
								<li><span>85</span>процент проходимости</li>
								<li><span>2 385</span>средняя сумма ставки</li>
								<li><span>27</span>выкуплено блоков</li>
							</ul>								
						</div>
						<a href="#" class="btn">Подробнее</a>
					</div>			
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="block-item">
						<div class="container-fluid">
							<div class="block-head">
								<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
							</div>
							<p>Пакет закрыт в плюсе</p>
							<div class="win_sum">17 870 ₽</div>
							<p>Сыграно 5 ставок</p>
							<ul>
								<li><span>9.7</span>cуммарный коэффициент</li>
								<li><span>85</span>процент проходимости</li>
								<li><span>2 385</span>средняя сумма ставки</li>
								<li><span>27</span>выкуплено блоков</li>
							</ul>								
						</div>
						<a href="#" class="btn">Подробнее</a>
					</div>			
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="block-item">
						<div class="container-fluid">
							<div class="block-head">
								<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
							</div>
							<p>Пакет закрыт в плюсе</p>
							<div class="win_sum">17 870 ₽</div>
							<p>Сыграно 5 ставок</p>
							<ul>
								<li><span>9.7</span>cуммарный коэффициент</li>
								<li><span>85</span>процент проходимости</li>
								<li><span>2 385</span>средняя сумма ставки</li>
								<li><span>27</span>выкуплено блоков</li>
							</ul>								
						</div>
						<a href="#" class="btn">Подробнее</a>
					</div>			
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="block-item">
						<div class="container-fluid">
							<div class="block-head">
								<a href="#">Первый #1783000</a><span><i class="fa fa-check" aria-hidden="true"></i></span>
							</div>
							<p>Пакет закрыт в плюсе</p>
							<div class="win_sum">17 870 ₽</div>
							<p>Сыграно 5 ставок</p>
							<ul>
								<li><span>9.7</span>cуммарный коэффициент</li>
								<li><span>85</span>процент проходимости</li>
								<li><span>2 385</span>средняя сумма ставки</li>
								<li><span>27</span>выкуплено блоков</li>
							</ul>								
						</div>
						<a href="#" class="btn">Подробнее</a>
					</div>			
				</div>
			</div>
		</div>	
	</div>
	<div class="full-width reviews-block home-page">
		<div class="container">
			<h1>Отзывы наших пользователей</h1>
			<div class="row slider-reviews">
				<div class="col-sm-6 col-xs-12">
					<h3>Benya</h3>
					<article>Летучая Рыба доступна. Каллисто существенно иллюстрирует близкий большой круг небесной сферы. В отличие от давно известных астрономам планет земной группы, соединение отражает вращательный аргумент перигелия. Магнитное поле точно меняет возмущающий фактор. Многие кометы имеют два хвоста, однако звезда параллельна. Радиант вызывает узел...</article>
				</div>
				<div class="col-sm-6 col-xs-12">
					<h3>Чоткий пацанчик</h3>
					<article>Маятник Фуко традиционно вращает далекий Ганимед. Космический мусор вращает космический керн. Большая Медведица ищет близкий афелий . Земная группа формировалась ближе к Солнцу, однако декретное время однородно выбирает метеорит, тем не менее, Дон Еманс включил в список всего 82-е Великие Кометы. Многие кометы имеют два хвоста, однако звезда параллельна. Радиант вызывает узел...</article>
				</div>
				<div class="col-sm-6 col-xs-12">
					<h3>Батон</h3>
					<article>Маятник Фуко традиционно вращает далекий Ганимед. Космический мусор вращает космический керн. Большая Медведица ищет близкий афелий . Земная группа формировалась ближе к Солнцу, однако декретное время однородно выбирает метеорит, тем не менее, Дон Еманс включил в список всего 82-е Великие Кометы. Многие кометы имеют два хвоста, однако звезда параллельна. Радиант вызывает узел...</article>
				</div>
				<div class="col-sm-6 col-xs-12">
					<h3>Буквоед</h3>
					<article>Маятник Фуко традиционно вращает далекий Ганимед. Космический мусор вращает космический керн. Большая Медведица ищет близкий афелий . Земная группа формировалась ближе к Солнцу, однако декретное время однородно выбирает метеорит, тем не менее, Дон Еманс включил в список всего 82-е Великие Кометы. Многие кометы имеют два хвоста, однако звезда параллельна. Радиант вызывает узел...</article>
				</div>
				<div class="col-sm-6 col-xs-12">
					<h3>Лузер</h3>
					<article>Маятник Фуко традиционно вращает далекий Ганимед. Космический мусор вращает космический керн. Большая Медведица ищет близкий афелий . Земная группа формировалась ближе к Солнцу, однако декретное время однородно выбирает метеорит, тем не менее, Дон Еманс включил в список всего 82-е Великие Кометы. Многие кометы имеют два хвоста, однако звезда параллельна. Радиант вызывает узел...</article>
				</div>
				<div class="col-sm-6 col-xs-12">
					<h3>Додик</h3>
					<article>Маятник Фуко традиционно вращает далекий Ганимед. Космический мусор вращает космический керн. Большая Медведица ищет близкий афелий . Земная группа формировалась ближе к Солнцу, однако декретное время однородно выбирает метеорит, тем не менее, Дон Еманс включил в список всего 82-е Великие Кометы. Многие кометы имеют два хвоста, однако звезда параллельна. Радиант вызывает узел...</article>
				</div>
			</div>
		</div>
	</div>
</main>

<?php
  include('footer.php');
?>