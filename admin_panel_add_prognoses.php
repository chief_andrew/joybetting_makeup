<?php
  include('header.php');
  include('admin_panel_menu.php');
?>
    <div class="container">
        <div id="forecas-control-panel">                    
            <form action="" method="POST" class="form-add-prognoses">
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="kind-of-sport">
                            <input id="football" class="hidden" type="radio" name="kind_of_sport" value="football" data-func="radio_input" checked>
                            <label for="football">
                                <span><i class="fa fa-futbol-o" aria-hidden="true"></i></span>
                                <span>Футбол</span>
                            </label>
                            <input id="hockey" class="hidden" type="radio" name="kind_of_sport" value="hockey" data-func="radio_input">
                            <label for="hockey">
                                <span><i class="fa fa-futbol-o" aria-hidden="true"></i></span>
                                <span>Хоккей</span>
                            </label>
                            <input id="basketball" class="hidden" type="radio" name="kind_of_sport" value="basketball" data-func="radio_input">
                            <label for="basketball">
                                <span><i class="fa fa-futbol-o" aria-hidden="true"></i></span>
                                <span>Баскетбол</span>
                            </label>
                        </div>                                
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="game_date">
                            <h4>Время</h4>                            
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker1').datetimepicker();
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="form-group period-block">
                            <h4>Период игры</h4>
                            <div class="period_the_game">
                                <input id="main_period" class="hidden" type="radio" name="period_the_game" value="main_period" data-func="radio_input" >
                                <label for="main_period" class="btn btn-primary hidden">
                                    <span class="main_period_btn">Основной</span>
                                </label>
                                <input id="period1" class="hidden" type="radio" name="period_the_game" value="period1" data-func="radio_input" >
                                <label for="period1" class="btn btn-default hidden">
                                    <span>1</span>
                                </label>
                                <input id="period2" class="hidden" type="radio" name="period_the_game" value="period2" data-func="radio_input" >
                                <label for="period2" class="btn btn-default hidden">
                                    <span>2</span>
                                </label>
                                <input id="period3" class="hidden" type="radio" name="period_the_game" value="period3" data-func="radio_input">
                                <label for="period3" class="btn btn-default hidden">
                                    <span>3</span>
                                </label>
                                <input id="period4" class="hidden" type="radio" name="period_the_game" value="period4" data-func="radio_input">
                                <label for="period4" class="btn btn-default hidden">
                                    <span>4</span>
                                </label>    
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="liga-block form-group">
                            <h4>Лига</h4>
                            <input type="text" class="form-control" id="liga-field" name="liga" placeholder=" Введите значение ">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="first-comand-block form-group">
                            <h4>1-я команда</h4>
                            <input type="text" class="form-control" id="first-comand" name="first-comand" placeholder=" Введите значение ">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="second-comand-block form-group">
                            <h4>2-я команда</h4>
                            <input type="text" class="form-control" id="second-comand" name="second-comand" placeholder=" Введите значение ">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="market-block form-group">
                            <h4>Рынок</h4>
                            <select name="market-option" class="form-control">
                            </select>   
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="rate-block form-group">
                            <h4>Ставка</h4>
                            <div class="rate-fields">   
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="coefficient-block form-group">
                            <h4>Коефициент</h4>
                            <input type="text" class="form-control" id="coefficient-field" name="coefficient" placeholder=" Введите значение ">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-5 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                        <div class="submit-block">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Пуск</button>
                        </div>
                    </div>
                </div>
                <script type="text/javascript" src="js/joybet_add_prognoses.js"></script>                        
            </form>                 
        </div>
    </div>
</main>
<?php
  include('footer.php');
?>