<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<form action="" method="POST" class="schet-form">
					<div class="row">
						<div class="col-sm-4 left-panel panel_height">
							<ul class="container-fluid">
								<li>
									<h3>Первый пакет</h3>
									<p>Счет #<span>1000734</span></p>
								</li>
								<li>
									<div class="fa-block">
										<i class="fa fa-shopping-bag" aria-hidden="true"></i>
									</div>
									<div class="info-block">
										<p>Пакетов</p>
										<span>30</span>
									</div>
								</li>
								<li>
									<div class="fa-block">
										<i class="fa fa-credit-card" aria-hidden="true"></i>
									</div>
									<div class="info-block">
										<p>К оплате</p>
										<span>23 000</span><span>р.</span>
									</div>
								</li>
								<li>
									<div class="fa-block">
										<i class="fa fa-calendar" aria-hidden="true"></i>
									</div>
									<div class="info-block">
										<p>Дата</p>
										<span>07.12.2016</span>
									</div>
								</li>
								<li>
									<div class="fa-block">
										<i class="fa fa-gift" aria-hidden="true"></i>
									</div>
									<div class="info-block">
										<p>Баллов</p>
										<span>1 377</span>
									</div>
								</li>
							</ul>
						</div>
						<div class="col-sm-8 right-panel panel_height">
							<div class="stsus-row paid">
								<span>Оплачен</span>
								<i class="fa fa-clock-o" aria-hidden="true"></i>
							</div>
							<div class="container-fluid">
								<div class="schet-stsus">
									<div class="form-group">
										<i class="fa fa-check-circle" aria-hidden="true"></i>
									</div>
									<div class="form-group">
										<p>Поздравляем! Счет оплачен.Удачной игры!</p>
										<a href="#">Мои пакеты</a>
									</div>
								</div>
							</div>							
						</div>
					</div>					
				</form>				
			</div>
		</div>
	</div>
</main>
<script src="js/buy_unit.js" type="text/javascript"></script>
<?php
  include('footer.php');
?>