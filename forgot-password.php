<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<form action="" mathod="POST" id="log-in" class="login-form">
					<h1>Забыли пароль?</h1>
					<div class="form-group">
						<p>Введите адрес электронной почты, чтобы сбросить пароль. Возможно, вам понадобится проверить папку нежелательной почты.</p>
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1">Адрес электронной почты</label>
						<input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email">
					</div>
					<div class="submit-box our-rules">
						<div class="row">
							<div class="col-md-6 submit-wrap">
								<button type="submit" class="btn btn-primary btn-lg btn-block">Отправить</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>