<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs">
				<?php include('sidebar.php');?>
				<?php include('banner.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">				
				<div class="page-block">
					<div class="block-title">
						<p>Первый пакет<span>#1783000</span><i class="fa fa-play-circle" aria-hidden="true"></i></p>
					</div>
					<span>В игре</span>	
					<h3>Статистика</h3>
					<span>Ожидаем первое событие</span>
					<h3>Правила и рекоммендации</h3>
					<div class="recommendations-list">
						<ul>
							<li><span>&bull;</span>Мы рекомендуем ставить в БК Pinnacle, так как у них выгодней коефф. Зарегистрируйтесь <a href="#">здесь</a>.</li>
							<li><span>&bull;</span>Старайтесь сделать ставку как можно быстрее, сразу после ее появления в блоке.</li>
							<li><span>&bull;</span>Во время создания ставки придерживайтесь рекоммендованной суммы ставки, в таблице (Р. Сумма).</li>
							<li><span>&bull;</span>Остались вопросы? Посетите раздел <a href="#">Помощь</a>.</li>
						</ul>
					</div>
					<h3>Прогнозы</h3>
					<table class="table table-prognoses table-striped table-bordered responsive">
						<thead>
							<tr>
								<th class="id_check">#</th>
								<th>Событие</th>
								<th>Ставка</th>
								<th>Коэф.</th>
								<th>Р.сумма</th>
								<th></th>
							</tr>
							<tr class="next-event-row">
								<td class="id_check next-event"></td>
								<td colspan="5" class="next-event">Ожидаем следующее событие...</td>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</main>
<script src="js/jquery.matchHeight.js" type="text/javascript"></script>

<?php
  include('footer.php');
?>