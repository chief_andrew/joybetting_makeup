<?php
  include('header.php');
?>
<main id="wrapper">
	<div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<li class="fa"><a href="#">Главная</a></li>
				<li class="fa"><a href="#">Предидущая</a></li>
				<li class="fa active"><span>Текущая</span></li>
			</ul>			
		</div>
		<div class="row account-block">
			<div class="col-lg-3 col-md-3 col-sm-3 sidebar-panel">
				<?php include('sidebar.php');?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<h1>Мои настройки</h1>
				<div class="row">
					<div class="col-md-6 col-xs-12">
						<form action="" mathod="POST" id="log-in" class="login-form">
							<h3>Изменение пароля</h3>
							<div class="form-group">
								<label for="exampleInputPassword1">Текущий пароль</label>
								<input type="password" class="form-control" id="user_password1" name="user_password1" placeholder="Enter your password">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword2">Новый пароль</label>
								<input type="password" class="form-control" id="new_user_password1" name="new_user_password1" placeholder="Enter new password">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword2">Новый пароль еще раз</label>
								<input type="password" class="form-control" id="new_user_password2" name="new_user_password2" placeholder="Enter new password agen">
							</div>
							<div class="submit-box">
								<div class="row">
									<div class="col-md-8 col-sm-6 col-xs-12 submit-wrap">
										<button type="submit" class="btn btn-primary btn-lg btn-block">Сохранить</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
	</div>
</main>
<?php
  include('footer.php');
?>