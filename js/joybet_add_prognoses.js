<!-- Периоды игры -->
var football = ["main_period", "period1", "period2"];
var hockey = ["main_period", "period1", "period2", "period3"];
var basketball = ["main_period", "period1", "period2", "period3", "period4"];
<!-- Рынок -->
var market = {
    '1x2' : ["1","X","2"],
    'Тотал(больше)' : ["more1", "more2", "more3"],
    'Тотал(меньше)' : ["under1", "under2", "under3"],
    'Всего голов' : ["1", "2", "3", "4", "5"]
};

function selected_sport_period(){
    if($(this).prop('checked')===true){                                    
        active_input = $(this);
        var period = window[active_input.val()];
        $('.period_the_game >.btn').each(function(index){
            if($(this).attr('for') == period[index]) {
                $(this).removeClass('hidden');
            } else {
                $(this).addClass('hidden');
            }                                    
            index++;
        });
    }
}

function select_rate(){
    var rate = $(".market-block>.form-control").find("option:selected");
    for( var key in market ) {
        if(rate.val() == key){
            $('.rate-fields').empty()
            $.each(market[key], function(index, value){

                var str  = '<label for="rate_'+value+'" class="btn btn-default">';
                str += '<span class="main_period_btn">'+value+'</span></label>';
                $('.rate-fields').append('<input id="rate_'+value+'" class="hidden" type="radio" name="rate" value="rate_'+value+'" data-func="radio_input">');
                $('.rate-fields').append(str);


            });										
        }
    }
}

$(document).ready(function(){								
    $('.kind-of-sport >[type=radio]').each(selected_sport_period);

    $.each(market, function( key, value ) {
        console.log( 'Свойство: ' +key);
        $('.market-block>.form-control').append('<option>'+key+'</option>');
    });
    select_rate();
});

$(".market-block>.form-control").on('change',function(){
    select_rate();
});	


$(".kind-of-sport>[type=radio]").click(selected_sport_period);