/*------- Главная страница --------*/

$('.how_do_we_get_results ul>li').on('click', function(){
	current = $(this);
	if(current.hasClass('active') === false ){
		current.addClass('active');
		current.find('.description').slideToggle(200);
	}else {
		current.removeClass('active');
		current.find('.description').slideToggle(200);
	}
});

////// замена логотипа ////////
var full_logo = 'images/footer_logo.png', // В эту переменную нужно будет засунуть адресс изображения
	mobile_logo = 'images/joybetting_light_blue.svg', // В эту переменную нужно будет засунуть адресс изображения для мобильных
	logo_wrap = $('.navbar-brand'),
	main_logo = $('.navbar-brand>img');

function chenge_logo() {
	if ( $(window).width()< 768 ){
		main_logo.attr('src', mobile_logo);
		logo_wrap.css('padding-top', '4px')
	} else {
		main_logo.attr('src', full_logo);
		logo_wrap.css('padding-top', '')
	}
	
}

$(document).ready(chenge_logo);
$(window).on('resize', function(){
	chenge_logo();
});


////////////  вызов Slick-Slider для отзывов ////////////
$(document).on('ready', function() {	
	$(".slider-reviews").slick({		
		dots: true,
		mobileFirst: true,
		arrows: false,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					infinite: true,
					dots: true
				}
			}
		]
	});
});	

//////////// Сервис, который вам по карману /////////////
$(document).ready(function() {
	var max_quantity_blocks = 31;
	
	var count = $('.sum-packege-control .count'),
		plus = $('.sum-packege-control .plus'),
		minus = $('.sum-packege-control .minus'),
		full_price = $('.price-block .full_price'),
		bonuses = $('.price-block .bonuses'),
		package_prise = 1000,
		one_peercent = package_prise / 100,
		step_percentage = 1,
		num_format = /(\d)(?=(\d{3})+([^\d]|$))/g;
	
	count.html(max_quantity_blocks);
	if(count.html() == max_quantity_blocks){
		plus.addClass('inaccessible');
	}
	
	function cost_calculation(){
		var count_value = count.html(),
			discount = (one_peercent * (count_value * (count_value - 1)) / 2) * step_percentage,
			old_prise = package_prise * count_value,
			new_prise = package_prise * count_value - discount,
			current_percentage = discount / (one_peercent * count_value);
		full_price.html(String(old_prise).replace(num_format, '$1 '));
		bonuses.html(String(discount).replace(num_format, '$1 '));
	}
	
	cost_calculation();
	
    plus.click(function() {
		minus.removeClass('inaccessible');
		if(count.html() < max_quantity_blocks ){
			count.html(+count.html()+1);
			cost_calculation();
		};
		if (count.html() == max_quantity_blocks) {
			$(this).addClass('inaccessible');
		}
    });
	minus.click(function() {
		plus.removeClass('inaccessible');
		if(count.html() > 1 ){
			count.html(+count.html()-1);
			cost_calculation();
		};
		if(count.html() == 1 ){
			$(this).addClass('inaccessible');
		}
    });	
});

