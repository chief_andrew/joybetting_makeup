var h1 = ("h1"),
form_control = $("form.control"),
check_box_row = $(".check_box_row"),
check_inputs = $("[type=radio], [type=checkbox]"),
control_block = $(".control_block"),
form_elem_control_blocks = control_block.find("input[type=radio], input[type=number], input[type=text], select"),
btn_control_blocks = control_block.find(".btn"),
inputs_control_blocks = control_block.find("input, button, select"),
input_text = control_block.find('input[type=text]');

inputs_control_blocks.each(function (index, elem) {
    elem.disabled = true;
});
check_inputs.each(function (index, elem) {
    elem.checked = false;
});
form_control.on("change input", function (e) {
    var cur_input = $(e.target);
    window['func_' + cur_input.data('func')](cur_input);
});
form_control.on("submit", function (e) {
    // e.preventDefault();
    btn_control_blocks.each(function (index, elem) {
        var elem = $(elem);
        if (elem.prop('disabled')) {
            elem.closest('.form-group').find('input, select, button').removeAttr('name');
        }
    });
});

$('div.alert').delay(5000).slideUp(300);
function disableSendButtons(inputs, buttons, check) {
    buttons.each(function (index, elem) {
        elem.disabled = true;
    });
    if (check) {
        inputs.each(function (index, elem) {
            var elem = $(elem);
            if (elem.prop('checked')) {
                elem.closest('.control_block').find('.btn').prop('disabled', false);
            }			
        });
    }	
}
//----//
function func_check_box_row(cur_input) {
    var disable_status,
            check;
    if (cur_input.prop('checked')) {
        tr_class = "marked";
        disable_status = false;
		var coment_text = cur_input.closest('tr').next('tr').find('p').text();
		input_text.attr('value', coment_text);
    } else {
        tr_class = "";
		input_text.attr('value', '');
        disable_status = true;
    }
    cur_input.closest("tr").attr('class', tr_class);
    check_box_row.each(function (index, elem) {
        if (elem.checked === true) {
            disable_status = false;
        }
    });
    check = !disable_status;
    form_elem_control_blocks.each(function (index, elem) {
        elem.disabled = disable_status;
    });
	
    disableSendButtons(form_elem_control_blocks, btn_control_blocks, check);
}
function func_radio_input(cur_input) {
    btn_control_blocks.each(function (index, elem) {
        elem.disabled = true;
    });
    cur_input.closest('.control_block').find(".btn").prop('disabled', false);	
}
function func_number_input(cur_input) {
    btn_control_blocks.each(function (index, elem) {
        elem.disabled = true;
    });
    if (cur_input.val() !== '') {
        cur_input.closest('.control_block').find(".btn").prop('disabled', false);
    }
}
function func_text_input(cur_input) {
	if (cur_input.val() !== ''){
		$('.coment-block').find('.btn').each(function (index, elem) {
			elem.disabled = true;
		});
		cur_input.closest('.coment-block').find(".btn").prop('disabled', false);
	} else {
		cur_input.closest('.coment-block').find(".btn").prop('disabled', true);
	}	    	
}
function func_select_input(cur_input) {
    return;
}


$('.output-coment').each(function(index, elem){
	var text_coment = $(this).find('p').text();		
	if(text_coment == ''){
		$(this).hide();
	}	
});

$('.output-coment').closest('.coment-string-row').hover(	
function(){
	$(this).css('background', '#FFF8F0');
	$(this).prev('tr').css('background', '#FFF8F0');
},
function(){
	$(this).css('background', '');
	$(this).prev('tr').css('background', '');
});

var form_filter = $('form.form-filter');

form_filter.on('submit', function (e) {
    var elems = $(this).find('input, select');
    elems.each(function (index, elem) {
        if (!elem.value) {
            elem.name = '';
        }
    });
});

//# sourceMappingURL=radio_btn.js.map