/*------- вызов м-да "Блоки одинаговой высоты" --------*/
$(document).ready(function() {
	$('.block-item, .type-pack-head label, .my_account .stat-block').matchHeight({
		byRow: true,
		property: 'height',
		remove: false
	});
});
/*------- Активация DateTimePicker на странице "Клиенты" --------*/
$(function(){
	$('#date_first, #date_second').datetimepicker({
		format: 'YYYY-MM-DD',
		pickTime: false,
		locale: 'ru',
		language: 'ru',
		icons: {
            time: false
		}
	});
	$("#date_first").on("dp.change",function (e) {
      $("#date_second").data("DateTimePicker").setMinDate(e.date);
    });
    //При изменении даты в date_second, она устанавливается как максимальная для date_firstr
    $("#date_second").on("dp.change",function (e) {
      $("#date_first").data("DateTimePicker").setMaxDate(e.date);
    });
});

/*+++++++++++++ H-E-A-D-E-R +++++++++++++*/
/*------- Шторка над шапкой --------*/

function blind_manipulation(){
	if($('.blind-wrap').length > 0){
		var img_wrap = $('.blind .image-box'),
			img = $('.blind .image-box>img'),
			img_wrap_parent_h = img_wrap.parent().height();
				
		img_wrap.height(img_wrap_parent_h);
		var img_top_position = (img_wrap.height() - img.height()) / 2;
		img.css('top', img_top_position);
	}
}
$(function(){
	blind_manipulation();
	if($('.blind-wrap').length > 0){
		var blind = $('.blind'),
			blind_arrow = $('.blind-arrow');	
		
		blind.css('display', 'none');
		
		$('.blind-arrow').on('click', function(){			
			blind.slideToggle(400);
			blind_manipulation();
			if(blind_arrow.hasClass('show-full-arrow')=== true){
				blind_arrow. removeClass('show-full-arrow');
			}else{
				blind_arrow. addClass('show-full-arrow');
			}
		});
	}
});

$(window).resize(blind_manipulation);
/*------- Поменять в шапке два меню местами --------*/

function castling(){
	var right2 = $('.navbar-right-second'),
		right1 = $('.navbar-right-first');
	if ( $(window).width() < 768 ) {		
		right1.after(right2);		
	} else {
		right2.after(right1);
	}
}
/*------- Выноска меню уведомлений в верхнюю панель и перемещение кнопки "Выход в общий список" --------*/

var notify_desc = $('.notifications_menu'),
	notify_desc_html = notify_desc.html(),
	caret = $('.caret').parent();	

function notify_movement(){
	if($('.notify_in_head').length > 0){
		var notify_mob = $('.notify_in_head'),
		
		navbar_nav = $('.logined-list');
		
		navbar_nav.find('li>.dropdown-menu').each(function(){
			var dropdown_list = $(this);
			var dropdown_width = dropdown_list.width();
			var parent_width = dropdown_list.parent().width();
			var drop_left = (dropdown_width - parent_width);

			dropdown_list.css({
				'left' : - drop_left
			});
		});

		if($(window).width() < 768){
			notify_desc.empty();
			notify_mob.html(notify_desc_html);

			var dropdown = notify_mob.find('.dropdown-menu'),
				full_width = $(window).width();
			dropdown.css({
				'width' : full_width,
				'left' : - notify_mob.offset().left
			});
			notify_mob.on('click', function(e){
				dropdown.slideToggle(200);
			});

			$('.navbar-toggle').on('click', function(){
				if(notify_mob.hasClass('open')){
					dropdown.slideToggle(200);
				}
			});	

			$('#logout').appendTo('#bs-example-navbar-collapse-1');

		} else {
			$('#logout').appendTo('#logout_wrap');
			notify_mob.empty();
			notify_desc.html(notify_desc_html);
		}
	}
} 


$(function(){
	castling();
	notify_movement();
});

$(window).resize(function(){
	castling();
	notify_movement();
});

caret.on('click', function(){
	var transformer = $(this).find('.caret');
	if(transformer.hasClass('rotate') === false){
		transformer.addClass('rotate');
	} else {
		transformer.removeClass('rotate');
	}	
});

/*------- Прижимаем футер к нижнему краю окна браузера --------*/
function footerToBottom() {
	var browserHeight = $(window).height(),
		footerOuterHeight = $('footer').outerHeight(true),
		wrapperHeight = $('#wrapper').outerHeight(true) + $('#header').height() - $('#wrapper').height() + 3;
		$('#wrapper').css({
			'min-height' : browserHeight - footerOuterHeight - wrapperHeight
		});
	};
	footerToBottom();
	$(window).resize(function () {
	footerToBottom();					 
});

/*-- Вызов плагина горизонтальной прокрутки Smooth Scroll --*/

function smooth_scroll_top_line() {	
	if($(window).width()< 768 ){
		$(".nav_md_right, .nav_md_left").smoothDivScroll({
			mousewheelScrolling: "allDirections",
			manualContinuousScrolling: false, //Бесконечный цикл
			touchScrolling:true,
			hotSpotScrolling: false
		});
	} else {
		$(".nav_md_right, .nav_md_left").smoothDivScroll("destroy");
	}
	if($(window).width()<= 1200 ){
		$(".accrue-points, #schet-payment-method, .statistic-row").smoothDivScroll({
			mousewheelScrolling: "allDirections",
			manualContinuousScrolling: false,
			touchScrolling:true,
			hotSpotScrolling: false
		});
	}
	else {
		$(".accrue-points, #schet-payment-method, .statistic-row").smoothDivScroll("destroy");
	}
	if($(window).width()<= 992 ){
		$(".statistic-row").smoothDivScroll({
			mousewheelScrolling: "allDirections",
			manualContinuousScrolling: false,
			touchScrolling:true,
			hotSpotScrolling: false
		});
	}
	else {
		$(".statistic-row").smoothDivScroll("destroy");
	}
	
}

$(document).ready(smooth_scroll_top_line);
$(window).resize(smooth_scroll_top_line);

/*------- Валидация форм --------*/
$(function(){
	$("#log-in").validate({
	    rules: {
			user_name: {
				required : true,				
			},
			user_email: {
				required : true,
				email: true
			},
			user_password1: {
				required : true,
				minlength : 8 
			},
			user_password2: {
				required : true,
				minlength : 8,
				equalTo: "#user_pass1"
			}
	    },
		messages: {
			user_name: {
				required: "Поле имя обязательно для заполнения"
			},
			user_email: {
				required : "Поле Email обязательно для заполнения",
				email: "Email введен некорректно"
			},
			user_password1: {
				required : "Поле Пароль обязательно для заполнения",
				minlength : "Пароль должен состоять минимум из 8 символов" 
			},
			user_password2: {
				required : "Поле Повторите пароль обязательно для заполнения",				
				equalTo: "Паролить не совпадают"
			}
		}
	});
});
/*============== "Личный кабинет" ==============*/
/*------- задаем высоту левой части "Баллы" --------*/
function purse_wrap_centring(){
	var left_block = $('.my_account .scoreboard .main'),
		purse_wrap = $('.my_account .scoreboard .main>.purse-wrap'),
		purse_wrap_height = purse_wrap.height(),
		left_block_height = left_block.height(),
		p_w_p = (left_block_height - purse_wrap_height) / 2 + 25; 
	
	purse_wrap.css({
		'top' :  p_w_p
	});
}

$(document).ready(purse_wrap_centring);
$(window).resize(purse_wrap_centring);

/*------- задаем высоту правой панели Счет формы --------*/

var maxHeight = 0;
$('.schet-form').find('.panel_height').each(function(e, i){
	var height = $(i).height();
	if(height > maxHeight ){
		maxHeight =height;
	}
});


$(document).ready(function(){
	if($(window).width() > 768){
		$('.schet-form').find('.panel_height').height(maxHeight );
	}
});
$(window).on('resize', function(){
	if($(window).width() > 768){
		$('.schet-form').find('.panel_height').height(maxHeight );
	} else {
		$('.schet-form').find('.panel_height').height();
	}
});

/*------- Страница "Мои Баллы" --------*/

// Если блок закрыт в плюсе меняем его цвет на зеленый
$('.points-score-table').find('.sum_of_points').each(function(){
	var sum_points = $(this),
		sum_points_text = sum_points.text();
	
	if(sum_points_text.charAt(0) == '+'){
		sum_points.css('color', '#009900');
	} 
});

/*------- Страница "Помощь" --------*/

$('.faq').find('.response-block>h3').on('click', function(){
	current = $(this).parent();
	if(current.hasClass('active') === false ){
		current.addClass('active');
		//current.find('.answer').removeClass('hidden');
		current.find('.answer').slideToggle(200);
	}else {
		current.removeClass('active');
		current.find('.answer').slideToggle(200);
	}
});
/*------- Локальное хранилище --------*/

$('.payment-method').change(function(){
	$(this).find('input[type="radio"]').each(function(){
		if($(this).prop('checked') === true){                                    
			find_checked_input = $(this).attr('value');	
			localStorageRadio = localStorage.setItem('pay_method', find_checked_input);	
		}
	});
});

var checked_input_radio = localStorage.getItem('pay_method');

$(document).ready(function(){
	if(checked_input_radio){
		$('.payment-method').find('input[type="radio"]').each(function(){
			if($(this).attr('value') == checked_input_radio){
				$(this).prop('checked', true);
			}
		});
	}
});


