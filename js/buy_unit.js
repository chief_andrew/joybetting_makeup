var packages_sum = 30,
	package_prise = 1000,
	one_peercent = package_prise / 100,
	step_percentage = 1,
	most_popular = 3,
	num_format = /(\d)(?=(\d{3})+([^\d]|$))/g,
	price_mass = [];

$(function(){											
	for (i = 1; i <= packages_sum; i++) {
		var discount = (one_peercent * (i * (i - 1)) / 2) * step_percentage;
		var old_prise = package_prise * i;
		var new_prise = package_prise * i - discount;
		var current_percentage = discount / (one_peercent * i);
		var popular = '';
		if(i == most_popular){
			popular = ' Самый популярный!';
		}
		price_mass.push({
			count: i,
			old_prise: old_prise,
			new_prise: new_prise,
			discount: discount,
			most_pop: popular,
			discount_percent: current_percentage
		});
	}
	
	for(var key in price_mass){
		if(price_mass[key].count == 1){
			$('.form-control').append('<option value="'+price_mass[key].count+'" selected>' + 
		price_mass[key].count+' пак.'+price_mass[key].most_pop+'</option>');
			$('.bonus-string').html('<div class="fa_block"><i class="fa fa-bell" aria-hidden="true"></i></div><div class="text_block"><p class="font-bold">Для начисления бонусов нужно купить минимум 2 пакета</p></div>');
			$('.old_price').hide();
			$('.current_price i').addClass('blue');
			$('.current_price i').text(String(price_mass[key].new_prise).replace(num_format, '$1 '));
		}else{
			$('.form-control').append('<option value="'+price_mass[key].count+'">' + 
		price_mass[key].count+' пак. - экономия '+String(price_mass[key].discount.toFixed()).replace(num_format, '$1 ')+'р.'+price_mass[key].most_pop+'</option>');
		}									
	}
});

$('.form-control').change(function(){
	var current_option = $(this).find('option:selected').val();
	$.each(price_mass, function(index,value){
		if(current_option == value.count && value.count == 1){
			$('.bonus-string').html('<div class="fa_block"><i class="fa fa-bell" aria-hidden="true"></i></div><div class="text_block"><p class="font-bold">Для начисления бонусов нужно купить минимум 2 пакета</p></div>');
			$('.old_price').hide();
			$('.current_price i').addClass('blue');
			$('.current_price i').text(String(value.new_prise).replace(num_format, '$1 '));
		}else if(current_option == value.count && value.count != 1){
			$('.bonus-string').html('<div class="fa_block"><i class="fa fa-plus" aria-hidden="true"></i></div><div class="text_block"><span>'+String(value.discount).replace(num_format, '$1 ')+'</span><p>бонусов на личный счет</p></div>');
			$('.old_price').show();
			$('.old_price i').text(String(value.old_prise).replace(num_format, '$1 '));
			$('.current_price i').removeClass('blue');
			$('.current_price i').text(String(value.new_prise).replace(num_format, '$1 '));
		}
	});
});

function chenge_output_text(){
	$('.pack-head').find('.type-pack-head').each(function(index){
		var this_radio = $(this).find('input[type="radio"]');
		if(this_radio.prop('checked') === true){
			var label_text = this_radio.siblings('label').find('h3').text();
			$('.output1').text(label_text);
		}
	});
}
$(window).on('load', function(){
	chenge_output_text();
});
$('.pack-head').change(function() {
	chenge_output_text();
});