var modalDialog = $('.modal-dialog');

$(document).find('.comment-link').click(function(e){
	var get_comment = $(this).next('.comment-content').text();
	var get_header = $(this).closest('tr').find('.item_id').text();

	$('.modal-title span').text(get_header);
	$('.modal-body p').text(get_comment);

	var offset = $(this).offset();		
	var modal_top_position = offset.top + $(this).parent().height();
	var modal_left_position = offset.left  - (modalDialog.width() / 2);

	if($(window).width() < 768) {
		modalDialog.css({
			'position' : 'absolute',
			'top' :  (offset.top + $(this).parent().height()) - $(window).scrollTop(),
			'left' : 0,
			'right' : 0,
			'margin' : 'auto'
		});
	} else {
		modalDialog.css({
			'position' : 'absolute',
			'top' :  modal_top_position - $(window).scrollTop(),
			'left' : modal_left_position
		});
	}
	console.log('C лева'+offset.left+'C верху'+offset.top);
	console.log(get_header);
});
