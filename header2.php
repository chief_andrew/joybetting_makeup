<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Bootstrap 101 Template</title>

        <!-- Bootstrap -->    
        <link href="css/bootstrap.css" rel="stylesheet">
        <!-- Bootstrap-datetimepicker --> 
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
        <!-- Custom styles -->
        <link href="css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- connect script jQuery -->
        <script type="text/javascript" src="js/jquery.js"></script>
    </head>
    <body>
        <header id="header">
            <nav class="navbar navbar-default" id="header_menu">
                <div class="container">
                    <div class="navbar-header">
                       <div class="notify_in_head dropdown"></div>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="images/logo.png" alt="Logotype" class="img-responsive">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">            
                        <ul class="nav navbar-nav navbar-right-second logined-list">
                            <li class="dropdown notifications_menu">
							    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" aria-hidden="true"></i><span class="num_of_notif"><i>4</i></span></a>
							    <ul class="dropdown-menu">
							    	<span class="corner"></span>
									<li class="notification_item">
						    			<a href="#">
						    				<div class="row">
						    					<div class="col-xs-3 icon">
						    						<img src="images/purse.png" alt="" class="img-responsive">
						    					</div>					    					
						    					<div class="col-xs-9 text">
						    						<div>Пакеты успешно активированы</div>
						    					</div>
						    				</div>
						    			</a>
						    		</li>
						    		<li class="notification_item">
						    			<a href="#">
						    				<div class="row">
						    					<div class="col-xs-3 icon">
						    						<img src="images/scroll.png" alt="" class="img-responsive">
						    					</div>					    					
						    					<div class="col-xs-9 text">
													<div>Первый пакет #7587223 Появился новый прогноз</div> 
						    					</div>
						    				</div>
						    			</a>
						    		</li>
						    		<li class="notification_item">
						    			<a href="#">
						    				<div class="row">
						    					<div class="col-xs-3 icon">
						    						<img src="images/for_sms.png" alt="" class="img-responsive">
						    					</div>					    					
						    					<div class="col-xs-9 text">
						    						<div>Для смс информирования добавьте номер телефона</div>
						    					</div>
						    				</div>
						    			</a>
						    		</li>
						    		<li class="notification_item">
						    			<a href="#">
						    				<div class="row">
						    					<div class="col-xs-3 icon">
						    						<img src="images/gift.png" alt="" class="img-responsive">
						    					</div>					    					
						    					<div class="col-xs-9 text">
						    						<div>Начислено 1 700 бонусов</div>
						    					</div>
						    				</div>
						    			</a>
						    		</li>
						    		<li class="empty_item">
						    			<a href="#">Нет новых уведомлений</a>
						    		</li>									
							    </ul>
							</li>
                            <li class="dropdown login_top_menu">
							    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span>Василий Пупкин</a>
							    <ul class="dropdown-menu">
							    	<span class="corner"></span>
									<li><a href="#">Мой кабинет</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Купить пакет</a></li>
									<li><a href="#">Моя история</a></li>
									<li><a href="#">Мои пакеты</a></li>
									<li><a href="#">Мои баллы</a></li>
									<li><a href="#">Мои настройки</a></li>
									<li role="separator" class="divider"></li>
									<li id="logout_wrap">
										<form action="" method="POST" id="logout">
											<input type="submit" name="logout" value="Выйти">
										</form>
						    		</li>
							    </ul>
							</li>
                        </ul>         
                        <ul class="nav navbar-nav navbar-right navbar-right-first">
                            <li><a href="#">Прайс</a></li>
                            <li><a href="#">Статистика</a></li>
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Помощь</a></li>
                        </ul>           
                    </div>
                </div>
            </nav>  
        </header>
        <?php include('footer.php');?>