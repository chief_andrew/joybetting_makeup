<?php
  include('header.php');
  include('admin_panel_menu.php');
  include('modal_coment.php');
?>
    <div class="container">
        <div id="forecas-control-panel">                    
            <form action="" method="POST" class="form-prognoses form-buying control">
                <div class="control_block">
				<div class="recalculate-block">
					<div class="title_h4">
                        <h4>Модерация заявок</h4>
                    </div>
                    <div class="label-wrap input-group">
                        <input id="win" class="hidden" type="radio" name="forecast_status" data-func="radio_input" value="win" disabled="disabled">
                        <label for="win" class="radio-label"><i class="fa fa-check-circle" aria-hidden="true"></i></label>
                        <input id="loss" class="hidden" type="radio" name="forecast_status" data-func="radio_input" value="loss" disabled="disabled">
                        <label for="loss" class="radio-label"><i class="fa fa-minus-circle" aria-hidden="true"></i></label>
                    </div>
                    <div class="recalculate" style="margin-left: -5px">
                        <button type="submit" name="action" class="btn btn-primary" value="recalculate" disabled="disabled">Активировать</button>
                    </div>
				</div>
                </div>
                <table class="table table-striped table-bordered responsive">
                    <thead>
                        <tr>
                            <th class="id_check"></th>
                            <th class="id_check">id</th>
                            <th>Пользователь</th>
                            <th>Дата и время</th>
                            <th>Вид</th>
                            <th>Блоков</th>
                            <th>Сумма</th>
                            <th>Коментарий</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="checkbox-cell id_check">
                            <label>
                                <input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
                                <span class="checkbox-custom">&#10004;</span>
                            </label>
                        </td>
                        <td class="id_check item_id link-cell"><a href="#">000001</a></td>
                        <td>
                            <div class="surname font-bold">Ivahnenko</div>
                        </td>
                        <td>
                            <div class="stat_info">
                                <span>07.12.2016</span>
                                <span>13.33</span>
                            </div>
                        </td>
                        <td>Блок</td>
                        <td>1</td>
                        <td class="font-bold">1000 р.</td>
                        <td class="comment-cell">
                        	<button type="button" class="comment-link" data-toggle="modal" data-target=".bs-example-modal-sm">Посмотреть</button>
                        	<div class="comment-content">Та это п*здец!</div>
                        </td>
                        <td class="fa-cell"><i class="fa fa-check-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <td class="checkbox-cell id_check">
                            <label>
                                <input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
                                <span class="checkbox-custom">&#10004;</span>
                            </label>
                        </td>
                        <td class="id_check item_id link-cell"><a href="#">000002</a></td>
                        <td>
                            <div class="surname font-bold">Kopach</div>
                        </td>
                        <td>
                            <div class="stat_info">
                                <span>13.12.2016</span>
                                <span>16.45</span>
                            </div>
                        </td>
                        <td>Блок</td>
                        <td>5</td>
                        <td class="font-bold">8700 р.</td>
                        <td class="comment-cell">
                        	<button type="button" class="comment-link" data-toggle="modal" data-target=".bs-example-modal-sm">Посмотреть</button>
                        	<div class="comment-content">Отличная игра, ждем в следующем пакете!</div>
                        </td>
                        <td class="fa-cell"><i class="fa fa-clock-o" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <td class="checkbox-cell id_check">
                            <label>
                                <input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
                                <span class="checkbox-custom">&#10004;</span>
                            </label>
                        </td>
                        <td class="id_check item_id link-cell"><a href="#">000003</a></td>
                        <td>
                            <div class="surname font-bold">Kiselov</div>
                        </td>
                        <td>
                            <div class="stat_info">
                                <span>21.12.2016</span>
                                <span>21.33</span>
                            </div>
                        </td>
                        <td>Блок</td>
                        <td>2</td>
                        <td class="font-bold">3000 р.</td>
                        <td class="comment-cell">
                        	<button type="button" class="comment-link" data-toggle="modal" data-target=".bs-example-modal-sm">Посмотреть</button>
                        	<div class="comment-content">А это полный п*здец!</div>
                        </td>
                        <td class="fa-cell"><i class="fa fa-minus-circle" aria-hidden="true"></i></td>
                    </tr>                            
                    </tbody>
                </table>
                <div class="container-fluid statistics-row">
                    <div class="statistics-string">
                        <div class="counter font-bold">7 777 заявок</div>
                        <ul class="pager nextprev">
                            <li class="disabled">
                                <span><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></span>
                            </li>
                            <li>
                                <a href="#" rel="next"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>    	
                
            </form>
            <script type="text/javascript" src="js/joybet.js"></script> 
            <script type="text/javascript" src="js/joybet_buying_prognoses.js"></script>                
        </div>
    </div>
</main>
<?php
  include('footer.php');
?>