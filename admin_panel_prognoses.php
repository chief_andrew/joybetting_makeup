<?php
  include('header.php');
  include('admin_panel_menu.php');
?>
    <div class="container">
        <div id="forecas-control-panel">                    
            <form action="" method="POST" class="form-prognoses control">
                <div class="control_block">
                    <div class="recalculate-block">
                    	<div class="title_h4">
							<h4>Рассчитать</h4>
						</div>
						<div class="label-wrap input-group">
							<input id="win" class="hidden" type="radio" name="forecast_status" data-func="radio_input" value="win" disabled="disabled">
							<label for="win" class="radio-label"><i class="fa fa-check-circle" aria-hidden="true"></i></label>
							<input id="loss" class="hidden" type="radio" name="forecast_status" data-func="radio_input" value="loss" disabled="disabled">
							<label for="loss" class="radio-label"><i class="fa fa-times-circle" aria-hidden="true"></i></label>
							<input id="return" class="hidden" type="radio" name="forecast_status" data-func="radio_input" value="return" disabled="disabled">
							<label for="return" class="radio-label"><i class="fa fa-dot-circle-o" aria-hidden="true"></i></label>    
						</div>
						<div class="recalculate">
							<button type="submit" name="action" class="btn  btn-primary" value="recalculate" disabled="disabled">Рассчитать</button>
						</div>
                    </div>
                    <div class="coment-block">
						<div class="input-text-wrap">
							<input type="text" class="form-control" name="coment" value="" data-func="text_input" placeholder="Комментарий">
						</div>
						<div class="recalculate">
							<button type="submit" name="action" class="btn btn-primary go_coment" value="go_coment" disabled="disabled">Go</button>
						</div>
                    </div>
                </div>
                <table class="table table-prognoses table-striped table-bordered responsive">
                    <tbody>
                        <tr>
                            <td class="checkbox-cell id_check">
                                <label>
                                    <input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
                                    <span class="checkbox-custom">&#10004;</span>
                                </label>
                            </td>
                            <td class="id_check link-cell"><a href="#">#1</a></td>
                            <td>
                                <div class="football kingdom">
                                    <h4>Майнц — Бавария</h4>
                                    <p><span><i class="fa fa-futbol-o" aria-hidden="true"></i></span>UEFA Champions League</p>
                                    <p>2016-12-01 21:30 MSK</p>
                                </div>
                            </td>
                            <td>
                                <div class="stat_info">
                                    <p>1X2</p>
                                    <p>Бавария</p>
                                </div>
                            </td>
							<td>4.850</td>
                            <td>4850</td>
                            <td><i class="fa fa-play-circle" aria-hidden="true"></i></td>
                        </tr>
						<tr class="coment-string-row">
							<td class="id_check"></td>
							<td class="id_check"></td>
							<td colspan="6">
								<div class="output-coment">
									<span><i class="fa fa-comment" aria-hidden="true"></i></span><p>Отличная игра, ждем в следующем пакете!</p>
								</div>
							</td>
						</tr>
                        <tr>
                            <td class="checkbox-cell id_check">
                                <label>
                                    <input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
                                    <span class="checkbox-custom">&#10004;</span>
                                </label>
                            </td>
                            <td class="id_check link-cell"><a href="#">#2</a></td>
                            <td>
                                <div class="football kingdom">
                                    <h4>Кан —  Дижон</h4>
                                    <p><span><i class="fa fa-futbol-o" aria-hidden="true"></i></span>UEFA Champions League</p>
                                    <p>2016-12-01 21:30 MSK</p>
                                </div>
                            </td>
                            <td>
                                <div class="stat_info">
                                    <p>1X2</p>
                                    <p>Бавария</p>
                                </div>
                            </td>
							<td>1.756</td>
                            <td>1900</td>
                            <td><i class="fa fa-exclamation-circle" aria-hidden="true"></i></td>
                        </tr>
						<tr class="coment-string-row">
							<td class="id_check"></td>
							<td class="id_check"></td>
							<td colspan="6">
								<div class="output-coment">
									<span><i class="fa fa-comment" aria-hidden="true"></i></span><p></p>
								</div>
							</td>
						</tr>
                        <tr>
                            <td class="checkbox-cell id_check">
                                <label>
                                    <input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
                                    <span class="checkbox-custom">&#10004;</span>
                                </label>
                            </td>
                            <td class="id_check link-cell"><a href="#">#3</a></td>
                            <td>
                                <div class="football kingdom">
                                    <h4>Блэк Уингз Линц — Дорнбирн</h4>
                                    <p><span><i class="fa fa-futbol-o" aria-hidden="true"></i></span>UEFA Champions League</p>
                                    <p>2016-12-01 21:30 MSK</p>
                                </div>
                            </td>
                            <td>
                                <div class="stat_info">
                                    <p>Тотал (больше)</p>
                                    <p>Бавария</p>
                                </div>
                            </td>
							<td>3.850</td>
                            <td>2020</td>
                            <td><i class="fa fa-check-circle" aria-hidden="true"></i></td>
                        </tr>
						<tr class="coment-string-row">
							<td class="id_check"></td>
							<td class="id_check"></td>
							<td colspan="6">
								<div class="output-coment">
									<span><i class="fa fa-comment" aria-hidden="true"></i></span><p>Отличная игра, ждем в следующем пакете!</p>
								</div>
							</td>
						</tr>
                        <tr>
                            <td class="checkbox-cell id_check">
                                <label>
                                    <input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
                                    <span class="checkbox-custom">&#10004;</span>
                                </label>
                            </td>
                            <td class="id_check link-cell"><a href="#">#4</a></td>
                            <td>
                                <div class="football kingdom">
                                    <h4>Майнц — Бавария</h4>
                                    <p><span><i class="fa fa-futbol-o" aria-hidden="true"></i></span>UEFA Champions League</p>
                                    <p>2016-12-01 21:30 MSK</p>
                                </div>
                            </td>
                            <td>
                                <div class="stat_info">
                                    <p>1X2</p>
                                    <p>Бавария</p>
                                </div>
                            </td>
							<td>2.700</td>
                            <td>4850</td>
                            <td><i class="fa fa-dot-circle-o" aria-hidden="true"></i></td>
                        </tr>
						<tr class="coment-string-row">
							<td class="id_check"></td>
							<td class="id_check"></td>
							<td colspan="6">
								<div class="output-coment">
									<span><i class="fa fa-comment" aria-hidden="true"></i></span><p>Та это п*здец!</p>
								</div>
							</td>
						</tr>
                    </tbody>
				</table>
				<div class="container-fluid statistics-row">
					<div class="statistics-string">
						<div class="counter font-bold">7 777 заявок</div>
						<ul class="pager nextprev">
							<li class="disabled">
								<span><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></span>
							</li>
							<li>
								<a href="#" rel="next"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
							</li>
						</ul>
					</div>
				</div>
            </form>
            <script type="text/javascript" src="js/joybet.js"></script>                 
        </div>
    </div>
</main>
<?php
  include('footer.php');
?>     