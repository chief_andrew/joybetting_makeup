<footer id="footer">
            <div class="container">
                <div class="row row-foot-menu">
                    <div class="col-md-3 col-sm-4 col-xs-12 logo-box">
                        <a href="#" class="foot-logo">
                            <img src="images/footer_logo.png" alt="Logotype" class="img-responsive">
                        </a>
                        <p>&copy; JoyBetting 2016 - Сервис спортивной аналитики</p>
                    </div>
                    <div class="col-md-3 col-sm-2 col-xs-6">
                        <ul class="foot-list">
                            <li><a href="#">Прайс</a></li>
                            <li><a href="#">Статистика</a></li>
                            <li><a href="#">Помощь</a></li>
                        </ul>       
                    </div>
                    <div class="col-md-3 col-sm-2 col-xs-6">
                        <ul class="foot-list">
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Оплата</a></li>
                            <li><a href="#">Отзывы</a></li>
                            <li><a href="#">Соглашение</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <p class="in_social_p">JoyBetting в соцсетях</p>
                        <ul class="foot-list social-link">
                            <li><a href="#"><i class="fa fa-vk" aria-hidden="true" style="color:#5B7AA8"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true" style="color:#125589"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook-square" aria-hidden="true" style="color:#3B5998"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true" style="color:#1DA1F2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="row row-payment">
                    <div class="col-md-12 col-xs-12">
                        <ul class="payment-systems">
                            <li><a href="#"><img src="images/verified-by-visa.png" alt="verified-by-visa" class="img-responsive"></a></li>
                            <li><a href="#"><img src="images/MasterCard-logo.png" alt="MasterCard" class="img-responsive"></a></li>
                            <li><a href="#"><img src="images/yandex-money.png" alt="yandex-money" class="img-responsive"></a></li>
                            <li><a href="#"><img src="images/qiwi.png" alt="qiwi-wallet" class="img-responsive"></a></li>
                            <li><a href="#"><img src="images/sberbank.png" alt="sberbank" class="img-responsive"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>        
        <!-- скрипт moment-with-locales.min.js для работы с датами -->
        <script type="text/javascript" src="js/moment-with-locales.min.js"></script>  
        <!-- connect Twitter Bootstrap 3 platform script -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- connect Bootstrap datetimepicker script -->
        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
        <!-- connect Jquery Form Validate -->
		<script type="text/javascript" src="js/jquery.validate.min.js"></script>
        <!-- connect Custom scripts -->
        <script type="text/javascript" src="js/custom.js"></script>
        <!--<script src="js/joybet.js"></script>  -->  
        <script type="text/javascript" src="js/responsive-tables.js"></script>
        <!-- jquery ui для пользовательских эффектов --> 
        <script type="text/javascript" src="js/jquery-ui.custom.min.js"></script>
        <!-- jquery mousewheel для возможности прокрутки с помощью колесика мыши --> 
        <script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
        <!-- kinetic для возможности прокрутки касанием (touch) -->
        <script type="text/javascript" src="js/jquery.kinetic.js"></script>
        <!-- плагин плавной горизонтальной прокрутки Smooth Scroll --> 
        <script type="text/javascript" src="js/jquery.smoothdivscroll.min.js"></script>         
        <!-- jquery плагин блоки одинаковой высоты -->
        <script src="js/jquery.matchHeight.js" type="text/javascript"></script>
        <!-- Карусель слайдер прокрутки контента -->
        <script src="js/slick.min.js" type="text/javascript"></script>
        <!-- Главная страница -->
        <script type="text/javascript" src="js/hp.script.js"></script>
        
        <!-- CSS горизонтальной прокрутки Smooth Scroll --> 
        <link href="css/smoothDivScroll.css" rel="stylesheet" type="text/css" />
    </body>
</html>