<?php
  include('header.php');
  include('admin_panel_menu.php');
  include('modal_coment.php');
?>
    <div class="container">
        <div id="forecas-control-panel">
            <form action="" method="GET" class="form-filter">
            	<div class="accrue-points">
					<div class="date-block form-group">
						<h4>Дата</h4>
						<input type='text' class="form-control" id='date_first' placeholder="ГГГГ-ММ-ДД"/>
						<span>-</span>
						<input type='text' class="form-control" id='date_second' placeholder="ГГГГ-ММ-ДД"/>
					</div>
					<div class="points-block form-group">
						<h4>Баллов</h4>						
						<input type="text" pattern="[0-9]{1,6}" title="(поле доджно содержать только цифры)" class="form-control" name="bla" value="" data-func="text_input" placeholder="XXXX"/>
						<span>-</span>
						<input type="text" pattern="[0-9]{1,6}" title="(поле доджно содержать только цифры)" class="form-control" name="bla2" value="" data-func="text_input" placeholder="XXXX"/>
					</div>
					<div class="packege-block form-group">
						<h4>Пакетов</h4>						
						<input type="text" pattern="[0-9]{1,6}" title="(поле доджно содержать только цифры)" class="form-control" name="bla-bla" value="" data-func="text_input" placeholder="XXXX"/>
						<span>-</span>
						<input type="text" pattern="[0-9]{1,6}" title="(поле доджно содержать только цифры)" class="form-control" name="bla-bla2" value="" data-func="text_input" placeholder="XXXX"/>
					</div>
					<div class="sum-block form-group">
						<h4>Сумма</h4>						
						<input type="text" pattern="[0-9]{1,6}" title="(поле доджно содержать только цифры)" class="form-control" name="bla-bla-bla" value="" data-func="text_input" placeholder="XXXX"/>
						<span>-</span>
						<input type="text" pattern="[0-9]{1,6}" title="(поле доджно содержать только цифры)" class="form-control" name="bla-bla-bla2" value="" data-func="text_input" placeholder="XXXX"/>
					</div>
					<div class="form-group">
						<button type="submit" name="action"  class="btn btn-primary" value="find">Найти</button>
					</div>
				</div>
            </form>                    
            <form action="" method="POST" class="form-prognoses form-clients control">                
                <div class="control_block">
                	<div class="coment-block">
                		<div class="title_h4">
							<h4>Начислить балы</h4>
						</div>
						<div class="input-text-wrap">
							<input type="text" class="form-control" name="coment" value="" data-func="text_input" placeholder="10 000">
						</div>
						<div class="recalculate">
							<button type="submit" name="action" class="btn btn-primary go_coment" value="go_coment" disabled="disabled">Go</button>
						</div>
                    </div>
                </div>
                <table class="table table-striped table-bordered responsive">
                    <thead>
                        <tr>
                            <th class="id_check"></th>
                            <th class="id_check">id</th>
                            <th>Пользователь</th>
                            <th>Дата</th>
                            <th>Баллов</th>
                            <th>Пакетов</th>
                            <th>Первых</th>
                            <th>Live</th>
                            <th>Сумма</th>
                        </tr>
                    </thead>
                    <tbody>
						<tr>
							<td class="checkbox-cell id_check">
								<label>
									<input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
									<span class="checkbox-custom">&#10004;</span>
								</label>
							</td>
							<td class="id_check item_id link-cell"><a href="#">000001</a></td>
							<td>
								<div class="surname font-bold">Ivahnenko</div>
							</td>
							<td>
								<div class="stat_info">
									<span>07.12.2016</span>
								</div>
							</td>
							<td>3</td>
							<td>1</td>
							<td>1</td>
							<td>1</td>
							<td class="font-bold">1000p.</td>
						</tr>
                        <tr>
							<td class="checkbox-cell id_check">
								<label>
									<input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
									<span class="checkbox-custom">&#10004;</span>
								</label>
							</td>
							<td class="id_check item_id link-cell"><a href="#">000002</a></td>
							<td>
								<div class="surname font-bold">Kiselov</div>
							</td>
							<td>
								<div class="stat_info">
									<span>25.10.2016</span>
								</div>
							</td>
							<td>3</td>
							<td>1</td>
							<td>2</td>
							<td>1</td>
							<td class="font-bold">800p.</td>
						</tr>
                   		<tr>
							<td class="checkbox-cell id_check">
								<label>
									<input data-func="check_box_row" value="3461" name="page_id_77777" class="check_box_row" type="checkbox">
									<span class="checkbox-custom">&#10004;</span>
								</label>
							</td>
							<td class="id_check item_id link-cell"><a href="#">000003</a></td>
							<td>
								<div class="surname font-bold">Pupkin</div>
							</td>
							<td>
								<div class="stat_info">
									<span>18.10.2015</span>
								</div>
							</td>
							<td>--</td>
							<td>4</td>
							<td>2</td>
							<td>1</td>
							<td class="font-bold">1800p.</td>
						</tr>        
                    </tbody>
                </table>
                <div class="container-fluid statistics-row">
                    <div class="statistics-string">
                        <div class="counter font-bold">7 777 заявок</div>
                        <ul class="pager nextprev">
                            <li class="disabled">
                                <span><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></span>
                            </li>
                            <li>
                                <a href="#" rel="next"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>    	
                
            </form>
            <script type="text/javascript" src="js/joybet.js"></script>
            <script type="text/javascript" src="js/joybet_buying_prognoses.js"></script>                
        </div>
    </div>
</main>
<?php
  include('footer.php');
?>